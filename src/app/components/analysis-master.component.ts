import { Component, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContentService } from '../service/content.service';
import { AnalysisService } from './analysis-master.service';
import { Observable, Subject } from 'rxjs/Rx';
import { DataTableDirective } from 'angular-datatables'; 
import { Angular2Csv } from 'angular2-csv';
import { differenceWith, isEqual } from 'lodash';

@Component({
  selector: 'app-post',
  templateUrl: 'analysis-master.component.html',
  providers: [AnalysisService]
})
export class AnalysisComponent{
 
   @ViewChild(DataTableDirective)
  datatableElement: DataTableDirective;  

   public getChanges(originalCollection: any, changedCollection: any): any {
     return differenceWith(changedCollection, originalCollection, isEqual);
   }

  contents: any[];
  pager: any = {};
  pagedItems: any =[];
  submitted = false;
  dataAn = {
    Analysis_id: '',
    Analysis: '',
    status: 'master',
    checkInput: 0,
    checkInput2: 0
  }
  dataAnalysis = {
    Analysis_id: '',
    Analysis: '',
    status: ''
  };

  Analysis_id = "";
  AnalysisById = [];
  idAnalysis = "";
 
  dtOptions: DataTables.Settings = {}; 
  dtTrigger: Subject<String> = new Subject(); 

  constructor(
    private analysisSer: AnalysisService,
    public router: Router) { }

  private ngOnInit(){
    this.analysisSer.getAllAnalysis().subscribe(data => {
      console.log(data);
      this.contents = data;
      this.dtTrigger.next();
      /* this.setPage(1); */
    });
    this.analysisSer.getMaxManyId().subscribe(data => {
      data.forEach(element => {
        console.log("data max id:" + JSON.stringify(element.max_analysis));
        let stringAboutChange = element.max_analysis.slice(3);
        stringAboutChange = +stringAboutChange + 1;

        let lengthStr = element.max_analysis.length; //length of str from max_param
        /* let lenghtNum = stringAboutChange.toString().length */
        let maxIdParam = element.max_analysis.slice(0, 3);

        let lengthOfZero = lengthStr - maxIdParam.length;
        lengthOfZero = stringAboutChange.toString().padStart(lengthOfZero, "0"); //add '0' basicly to length of str - maxIdParam.length
        this.idAnalysis = maxIdParam + lengthOfZero;
        console.log("new data" + JSON.stringify(this.idAnalysis));
      });
    });
  }
  allItems: any[];
  downloadAnalysis() {
    this.analysisSer.getAllAnalysis().subscribe(data => {
    this.allItems = data;
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: false,
      headers: ['Analysis_id', 'Analysis', 'status']
    };
    new Angular2Csv(this.allItems, 'Data Analysis', options);
  });
}
  //function upload 
  showUpload = false;
  fileUpload() {
    this.showUpload = true;
  }
  cancelUpload() {
    this.showUpload = false;
  }
  
  text: any;
  JSONData: any;
  convertFile(input) {
    const reader = new FileReader();
    reader.readAsText(input.files[0]);
    reader.onload = () => {
      let text = reader.result;
      this.text = text;
      console.log(text);
      this.csvJSON(text);
    };
  }
  check: any = [];
  result: any = [];
  csvJSON(csvText) {
    var lines = csvText.split('\r\n');

    var headers = lines[0].split(",");
    console.log(headers);
    for (var i = 1; i < lines.length - 1; i++) {

      var obj = {};
      var currentline = lines[i].split(",");

      for (var j = 0; j < headers.length; j++) {
        obj[headers[j]] = currentline[j];
      }

      this.result.push(obj);
    }

    let updateItem = this.contents.find(
      this.findIndexToUpdate,
      this.result.Analysis_id
    );

    let index = this.contents.indexOf(updateItem);

    this.contents[index] = this.result;
    console.log(
      "alert analysis latest:" + JSON.stringify(this.result)
    );
    console.log(JSON.stringify(this.contents));
    this.JSONData = JSON.stringify(this.contents);
  }
  findIndexToUpdate(result: any) {
    return result.Analysis_id === this;
  }

  submittedbutton = false;
  save() {
    console.log("data check" + JSON.stringify(this.result));
    this.submittedbutton = true;
    let observables = new Array();
    let changes = this.getChanges(this.contents, this.result);

    if (this.result.length !== this.contents.length) {
      this.analysisSer.postAnalysis(changes)
        .subscribe(data => {
          alert("Your data has been added in database")
        }, window.location.reload());
    } else {
      for (this.dataAnalysis of this.result) {
        observables.push(this.analysisSer.putAnalisis(this.dataAnalysis));
        
        console.log("DATA ANALYSIS SEHABIS DIUPLOAD : " + JSON.stringify(this.dataAnalysis));
      }

      Observable.forkJoin(observables).subscribe(
        res => console.log(res),
        error => console.log("Error: ", error)
      );
    }
  }
 

  //add data
  anAvail = null
  addNewAnalysis() {
    this.submitted = true;

    let Analysis = this.dataAn.Analysis
    Analysis = Analysis.charAt(0).toUpperCase() + Analysis.slice(1)
    this.dataAn.Analysis = Analysis

    let Analysis_id = this.idAnalysis
    this.dataAn.Analysis_id = Analysis_id

    this.analysisSer.getAllAnalysis()
      .subscribe(data => {
        let count = 0
        data.forEach(element => {
          if (Analysis.toLowerCase() === element.Analysis.toLowerCase()) {
            count++
          }

        });

        if (count > 0) {
          this.anAvail = true;
          setTimeout(() => {
            this.anAvail = false;
          }, 5000)

          this.submitted = false
        }
        else {
          this.analysisSer.postAnalysis(this.dataAn)
            .subscribe(data => {
              alert("Your data has been added in database")
            }, window.location.reload()
            );
        }
      })
  }

  // function delete
  contentTitleWillBeDeleted = "";
  dataContentToBeDeleted = { content: '', item: '' }
  willDeleteContent(content, item) {
    this.contentTitleWillBeDeleted = item.Analysis;
    this.dataContentToBeDeleted.content = content;
    this.dataContentToBeDeleted.item = item;
  }
  deleteContent() {
    this.analysisSer.deleteAnalysis(this.dataContentToBeDeleted.item["Analysis_id"])
      .subscribe(data => {
        alert("Your data has been deleted from database")
      },
      window.location.reload()
    )
  }

  // function edit
  showEditContent = false;
  analisis:any;
  dataCurrAnalisi: any;
  cancelEdit() {
    this.showEditContent = false;
  }
  willEditContent(content, item) {
    console.log(content, item);
    this.showEditContent = true;
    this.analisis = item;
  }
  editContent() {
    this.submitted = true;

    let Analysis = this.analisis.Analysis
    Analysis = Analysis.charAt(0).toUpperCase() + Analysis.slice(1)
    this.analisis.Analysis = Analysis

    this.analysisSer.getAllAnalysis()
      .subscribe(data => {
        let count = 0
        data.forEach(element => {
          if (Analysis.toLowerCase() === element.Analysis.toLowerCase()) {
            count++
          }
        });

        if (count > 0) {
          this.anAvail = true;
          setTimeout(() => {
            this.anAvail = false;
          }, 5000)

          this.submitted = false
        }
        else {
          this.analysisSer.putAnalisis(this.analisis)
            .subscribe(data => {
              alert("Your data has been updated in database")
            }
            );
        }

      })
  }

 

}