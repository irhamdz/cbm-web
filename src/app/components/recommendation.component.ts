import { Component,ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { RecommendationService } from './recommendation.service';
import { Observable, Subject } from 'rxjs/Rx';
import { DataTableDirective } from 'angular-datatables'; 
import { Angular2Csv } from 'angular2-csv';
import { differenceWith, isEqual } from 'lodash';

@Component({
  selector: 'app-post',
  templateUrl: 'recommendation.component.html',
  providers: [RecommendationService]
})
export class RecommendationComponent {

@ViewChild(DataTableDirective)
  datatableElement: DataTableDirective; 
   dtOptions: DataTables.Settings = {}; 
  dtTrigger: Subject<String> = new Subject(); 


  contents: any[];
  pager: any = {};
  pagedItems: any[];
  idRecom = "";
  constructor(
    private recomServ: RecommendationService,
    public router: Router) {}

  private ngOnInit() {
    this.recomServ.getRecom().subscribe(data => {
      console.log(data);
      this.contents = data;
      this.dtTrigger.next();
      /* this.setPage(1); */
    });

    this.recomServ.getMaxManyId().subscribe(data => {
      data.forEach(element => {
        console.log("data max id:" + JSON.stringify(element.max_rec));
        let stringAboutChange = element.max_rec.slice(3);
        stringAboutChange = +stringAboutChange + 1;

        let lengthStr = element.max_rec.length; //length of str from max_param
        /* let lenghtNum = stringAboutChange.toString().length */
        let maxIdParam = element.max_rec.slice(0, 3);

        let lengthOfZero = lengthStr - maxIdParam.length;
        lengthOfZero = stringAboutChange.toString().padStart(lengthOfZero, "0"); //add '0' basicly to length of str - maxIdParam.length
        this.idRecom = maxIdParam + lengthOfZero;
        console.log("new data" + JSON.stringify(this.idRecom));
      });
    });
  }
  public getChanges(originalCollection: any, changedCollection: any): any {
    return differenceWith(changedCollection, originalCollection, isEqual);
  }
  submitted = false;
  dataRecom = {
    Recommendation_id: '',
    Recommendation: '',
    status: 'master',
    checkInput: 0,
    checkInput2: 0
  }
  dataRecommendation = {
    Recommendation_id: '',
    Recommendation: '',
    status: ''
  };
  allItems: any[];
  downloadRecom() {
    this.recomServ.getRecom().subscribe(data => {
      this.allItems = data;
      var options = {
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalseparator: '.',
        showLabels: true,
        showTitle: false,
        headers: ['Recommendation_id', 'Recommendation', 'status']
      };
      new Angular2Csv(this.allItems, 'Data Recom', options);
    });
  }

  text: any;
  JSONData: any;
  convertFile(input) {
    const reader = new FileReader();
    reader.readAsText(input.files[0]);
    reader.onload = () => {
      let text = reader.result;
      this.text = text;
      console.log(text);
      this.csvJSON(text);
    };
  }
  check: any = [];
  result: any = [];
  csvJSON(csvText) {
    var lines = csvText.split('\r\n');

    var headers = lines[0].split(",");
    console.log(headers);
    for (var i = 1; i < lines.length - 1; i++) {

      var obj = {};
      var currentline = lines[i].split(",");

      for (var j = 0; j < headers.length; j++) {
        obj[headers[j]] = currentline[j];
      }

      this.result.push(obj);
    }

    let updateItem = this.contents.find(
      this.findIndexToUpdate,
      this.result.Recommendation_id
    );

    let index = this.contents.indexOf(updateItem);

    this.contents[index] = this.result;
    console.log(
      "data recom latest:" + JSON.stringify(this.result)
    );
    console.log(JSON.stringify(this.contents));
    this.JSONData = JSON.stringify(this.contents);
  }
  findIndexToUpdate(result: any) {
    return result.Recommendation_id === this;
  }
  
  submittedbutton = false;
  save() {
    
    console.log("data check" + JSON.stringify(this.result));
    this.submittedbutton = true;
    let observables = new Array();
    let changes = this.getChanges(this.contents, this.result);

    if (this.result.length !== this.contents.length) {
      this.recomServ.postRecom(changes)
        .subscribe(data => {
          alert("Your data has been added in database")
        }, window.location.reload());
    } else {


      for (this.dataRecommendation of this.result) {
        observables.push(this.recomServ.putRecom(this.dataRecommendation));
        console.log("DATA ANALYSIS SEHABIS DIUPLOAD : " + JSON.stringify(this.dataRecommendation));
      }

      Observable.forkJoin(observables).subscribe(
        res => console.log(res),
        error => console.log("Error: ", error)
      );
    }
  }

  //add data
  anAvail = null
  addNewAnalysis() {
    this.submitted = true;

    let Recommendation_id = this.idRecom
    this.dataRecom.Recommendation_id = Recommendation_id

    let Recommendation = this.dataRecom.Recommendation
    Recommendation = Recommendation.charAt(0).toUpperCase() + Recommendation.slice(1)
    this.dataRecom.Recommendation = Recommendation

    this.recomServ.getRecom()
      .subscribe(data => {
        let count = 0
        data.forEach(element => {
          if (Recommendation.toLowerCase() === element.Recommendation.toLowerCase()) {
            count++
          }

        });

        if (count > 0) {
          this.anAvail = true;
          setTimeout(() => {
            this.anAvail = false;
          }, 5000)

          this.submitted = false
        }
        else {
          this.recomServ.postRecom(this.dataRecom)
            .subscribe(data => {
              alert("Your data has been added in database")
            },
            window.location.reload()
            );
        }
      })
  }

  // function delete
  contentTitleWillBeDeleted = "";
  dataContentToBeDeleted = { content: '', item: '' }
  willDeleteContent(content, item) {
    this.contentTitleWillBeDeleted = item.Recommendation;
    this.dataContentToBeDeleted.content = content;
    this.dataContentToBeDeleted.item = item;
  }
  deleteContent() {
    this.recomServ.deleteRecom(this.dataContentToBeDeleted.item["Recommendation_id"])
      .subscribe(data => {
        alert("Your data has been deleted from database")
      },
      window.location.reload()
      )
  }

  // function upload
  showUpload = false;
  fileUpload() {
    this.showUpload = true;
  }
  cancelUpload() {
    this.showUpload = false;
  }

  // function edit
  showEditContent = false;
  recom: any;
  cancelEdit() {
    this.showEditContent = false;
  }
  willEditContent(content, item) {
    console.log(content, item);
    this.showEditContent = true;
    this.recom = item;
  }
  
  editContent() {
    this.submitted = true;

    let Recommendation = this.recom.Recommendation
    Recommendation = Recommendation.charAt(0).toUpperCase() + Recommendation.slice(1)
    this.recom.Recommendation = Recommendation

    this.recomServ.getRecom()
      .subscribe(data => {
        let count = 0
        data.forEach(element => {
          if (Recommendation.toLowerCase() === element.Recommendation.toLowerCase()) {
            count++
          }
        });

        if (count > 0) {
          this.anAvail = true;
          setTimeout(() => {
            this.anAvail = false;
          }, 5000)

          this.submitted = false
        }
        else {
          this.recomServ.putRecom(this.recom)
            .subscribe(data => {
              alert("Your data has been updated in database")
            },
            window.location.reload()
            );
        }

      })
  }



  

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    // get pager object from service
    this.pager = this.recomServ.getPager(this.contents.length, page);

    // get current page of items
    this.pagedItems = this.contents.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
}
