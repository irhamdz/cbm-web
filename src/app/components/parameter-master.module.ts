import { PcarService } from './../service/pcar.service';
import { ParameterMasterComponent } from './parameter-master.component';
import { ParameterService } from './parameter-master.service';
import {NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';


@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule
    ],
    declarations: [ParameterMasterComponent],
    providers: [ParameterService,PcarService],
    bootstrap: [ParameterMasterComponent],
})
export class RecomModule{
}