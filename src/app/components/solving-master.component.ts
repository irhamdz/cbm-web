import { ParameterService } from "./parameter-master.service";
import { UnitConversionService } from "./../service/unit-conversion.service";
import { SolvingRecomServ } from "./../service/solving-recom.service";
import { SolvingAnalysisServ } from "./../service/solving-analysis.service";
import { PcarService } from "./../service/pcar.service";
import { Component, OnInit,ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, Subject } from "rxjs/Rx";
import { DataTableDirective } from "angular-datatables";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

@Component({
  templateUrl: "solving-master.component.html"
})
export class SolvingMasterComponent implements OnInit{
  newIdThreshold = "";
  newIdSolving = "";
  newIdParameter = "";
  parameterData = [];
  analysisData = [];
  recommenData = [];
  allSolvMaster = [];
  allUnitConversion = [];
  allTrendData = [];
  trendNameBySource = [];
  param: any;
  caution_top: number;
  caution_bottom: number;
  critical_top: number;
  critical_bottom: number;

  //disable select parameter
  canSelectParameter = false;

  //checkbox top and bottom
  top = false;
  bottom = false;
  top_bot_required = true;

  //alert
  submitted = false;
  anAvail = false;
  analysisNotSelected = false;
  recommendationNotSelected = false;
  paramAvail = false;
  countThresTop = false;
  countThresBot = false;

  selectedRecommendation = [];
  selectedAnalysis = [];

  //model parameter
  dataParameter = {
    Parameter_id: "",
    Parameter: "",
    Component: "",
    source: "",
    trend_id: null,
    combination: null,
    uom: ""
  };

  //model solving data Analysis
  dataAnalysis = {
    Solving_id: "",
    Threshold_id: "",
    Analysis_id: ""
  };

  //model solving data recommendation
  dataRec = {
    Solving_id: "",
    Threshold_id: "",
    Recommendation_id: ""
  };

  //model Threshold
  dataThres = {
    Threshold_id: "",
    Parameter_id: "",
    Criticality: "",
    Limit_Value: 0,
    model: "",
    priority: 1
  };

  defaultIdSolving = "SLV0000001";
  defaultIdParameter = "PRM0000001";
  defaultIdThreshold = "THR0000001";

  @ViewChild(DataTableDirective) datatableElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<String> = new Subject();

   //multiselect
    dropdownList = [];
    selectedItems = [];
    dropdownSettings = {};

  constructor(
    private pcarService: PcarService,
    private solvingAnService: SolvingAnalysisServ,
    private solvingRecService: SolvingRecomServ,
    private unitConv: UnitConversionService,
    private paramService: ParameterService
  ) {
    this.dtOptions = { pagingType: "full_numbers" };
  }

   ngOnInit() {
    //get auto generate max param
    this.paramService.getMaxManyId().subscribe(data => {
      console.log("all max many master data");
      console.log(data);
      data.forEach(element => {
        if (element.max_param != null) {
          console.log("data max id:" + JSON.stringify(element.max_param));
          let stringAboutChange = element.max_param.slice(3);
          stringAboutChange = +stringAboutChange + 1;

          let lengthStr = element.max_param.length; //length of str from max_param
          /* let lenghtNum = stringAboutChange.toString().length */
          let maxIdParam = element.max_param.slice(0, 3);

          let lengthOfZero = lengthStr - maxIdParam.length;
          lengthOfZero = stringAboutChange
            .toString()
            .padStart(lengthOfZero, "0"); //add '0' basicly to length of str - maxIdParam.length
          this.newIdParameter = maxIdParam + lengthOfZero;
          console.log("new id parameter" + JSON.stringify(this.newIdParameter));
        } else {
          this.newIdParameter = this.defaultIdParameter;
          console.log(
            "id baru parameter :" + JSON.stringify(this.newIdParameter)
          );
        }
      });
    });
    // get auto generate max id solving
    this.pcarService.getMaxSolv().subscribe(data => {
      console.log(data);
      data.forEach(element => {
        if (element.Solving_max != null) {
          console.log("data max id:" + JSON.stringify(element.Solving_max));
          let stringAboutChange = element.Solving_max.slice(3);
          stringAboutChange = +stringAboutChange + 1;

          let lengthStr = element.Solving_max.length; //length of str from max_solv
          /* let lenghtNum = stringAboutChange.toString().length */
          let maxIdSolv = element.Solving_max.slice(0, 3);

          let lengthOfZero = lengthStr - maxIdSolv.length;
          lengthOfZero = stringAboutChange
            .toString()
            .padStart(lengthOfZero, "0"); //add '0' basicly to length of str - maxIdSolv.length
          this.newIdSolving = maxIdSolv + lengthOfZero;
          console.log("new id solv" + JSON.stringify(this.newIdSolving));
        } else {
          this.newIdSolving = this.defaultIdSolving;
          console.log("ID BARU SOLVING :" + JSON.stringify(this.newIdSolving));
        }
      });
    });

    //get auto generate max id threshold
    this.pcarService.getMaxThres().subscribe(data => {
      console.log(data);
      data.forEach(element => {
        if (element.max_threshold != null) {
          console.log("data max id:" + JSON.stringify(element.max_threshold));
          let stringAboutChange = element.max_threshold.slice(3);
          stringAboutChange = +stringAboutChange + 1;

          let lengthStr = element.max_threshold.length; //length of str from max_threshold
          /* let lenghtNum = stringAboutChange.toString().length */
          let maxIdThres = element.max_threshold.slice(0, 3);

          let lengthOfZero = lengthStr - maxIdThres.length;
          lengthOfZero = stringAboutChange
            .toString()
            .padStart(lengthOfZero, "0"); //add '0' basicly to length of str - maxIdThres.length
          this.newIdThreshold = maxIdThres + lengthOfZero;
          console.log("new id threshold" + JSON.stringify(this.newIdThreshold));
        } else {
          this.newIdThreshold = this.defaultIdThreshold;
          console.log(
            "ID BARU threshold :" + JSON.stringify(this.newIdThreshold)
          );
        }
      });
    });

    //get All Parameter
    this.pcarService.getAllParameter().subscribe(data => {
      this.parameterData = data;
      console.log("all parameter data");
      console.log(this.parameterData);
    });

    //get All Analysis Master
    this.pcarService.getAllAnalysis().subscribe(data => {
      this.analysisData = data;
      console.log("all analysis data");
      console.log(this.analysisData);
    });

    //get All Recommendation Master
    this.pcarService.getAllRecommendation().subscribe(data => {
      this.recommenData = data;
      console.log("all recommendation data");
      console.log(this.recommenData);
    });

    //get All VW Solving Master With Desc
    this.pcarService.getAllSolvingMasterWithDesc().subscribe(data => {
      this.allSolvMaster = data;
      this.dtTrigger.next();
      console.log("all vw solving master data");
      console.log(this.allSolvMaster);
    });

    //get All Unit Conversions Data
    this.unitConv.getAllUnitConversion().subscribe(data => {
      this.allUnitConversion = data;
      console.log("all unit conversion data");
      console.log(this.allUnitConversion);
    });

    //get All Source Trend Data
    this.pcarService.getAllSourceTrend().subscribe(data => {
      this.allTrendData = data;
      console.log("all source trend data");
      console.log(this.allTrendData);
    });

    /* //get All Trend Data
    this.pcarService.getAllTrendData().subscribe(data => {
      this.allTrendData = data;
      console.log("all trend data");
      console.log(this.allTrendData);
    }); */
  


  //multiselect start here

    this.dropdownList = [
                              {"id":1,"itemName":"India"},
                              {"id":2,"itemName":"Singapore"},
                              {"id":3,"itemName":"Australia"},
                              {"id":4,"itemName":"Canada"},
                              {"id":5,"itemName":"South Korea"},
                              {"id":6,"itemName":"Germany"},
                              {"id":7,"itemName":"France"},
                              {"id":8,"itemName":"Russia"},
                              {"id":9,"itemName":"Italy"},
                              {"id":10,"itemName":"Sweden"}
                            ];
        this.selectedItems = [
                                {"id":2,"itemName":"Singapore"},
                                {"id":3,"itemName":"Australia"},
                                {"id":4,"itemName":"Canada"},
                                {"id":5,"itemName":"South Korea"}
                            ];
        this.dropdownSettings = { 
                                  singleSelection: false, 
                                  text:"Select Countries",
                                  selectAllText:'Select All',
                                  unSelectAllText:'UnSelect All',
                                  enableSearchFilter: true,
                                  classes:"myclass custom-class"
                                };            
   }
    onItemSelect(item:any){
        console.log(item);
        console.log(this.selectedItems);
    }
    OnItemDeSelect(item:any){
        console.log(item);
        console.log(this.selectedItems);
    }
    onSelectAll(items: any){
        console.log(items);
    }
    onDeSelectAll(items: any){
        console.log(items);
    }


  onChangeSource(index, item) {
    console.log(item);
    /* this.param = item; */
    /*  let Parameter_id = item.Parameter_id;
    let Parameter = item.Parameter; */
    this.pcarService.getParameterBySource(item).subscribe(data => {
      this.trendNameBySource = data;
      console.log(this.trendNameBySource);
      this.canSelectParameter = true;
    });
  }

  onChangeParameter(index, item) {
    /* this.param = item; */
    /*  let Parameter_id = item.Parameter_id;
    let Parameter = item.Parameter; */
    let Parameter = item.trend_name;
    let Trend_id = item.id;
    let Source = item.trend_source;

    this.dataParameter.Parameter = Parameter;
    this.dataParameter.trend_id = Trend_id;
    this.dataParameter.source = Source;
    console.log(this.dataParameter);
  }

  changeTop() {
    this.top = !this.top; // this will change value of it true and false

    if (this.top || this.bottom) {
      this.top_bot_required = false;
    }

    if (!this.top && !this.bottom) {
      this.top_bot_required = true;
    }
    console.log(this.top);
  }

  changeBot() {
    this.bottom = !this.bottom; // this will change value of it true and false

    if (this.top || this.bottom) {
      this.top_bot_required = false;
    }
    if (!this.top && !this.bottom) {
      this.top_bot_required = true;
    }
    console.log(this.bottom);
  }

  save() {
    this.submitted = true;
    let observable = new Array();
    let recommendat = new Array();

    this.dataThres.Threshold_id = this.newIdThreshold;
    this.dataThres.Parameter_id = this.newIdParameter;
    this.dataParameter.Parameter_id = this.newIdParameter;
    console.log(this.dataParameter);
    this.paramService.getPara().subscribe(data => {
      let countParam = 0;
      console.log("data parameter master:" + JSON.stringify(data));
      data.forEach(element => {
        console.log("data element:" + JSON.stringify(element.source));
        if (
          this.dataParameter.source === element.source &&
          this.dataParameter.Parameter === element.Parameter &&
          this.dataParameter.Component.toLowerCase() === element.Component.toLowerCase() &&
          this.dataParameter.uom.toLowerCase() === element.uom.toLowerCase()
        ) {
          countParam++;
        }
      });

      this.pcarService.getThres().subscribe(datas => {
        let countThres = 0;
        let countThres_top = 0;
        let countThres_bot = 0;
        console.log("data threshold master:" + JSON.stringify(datas));
        if (this.top && !this.bottom) {
          this.dataThres.Criticality = "Caution Top";
          this.dataThres.Limit_Value = this.caution_top;
          datas.forEach(element => {
            if (
              this.dataThres.Parameter_id.toLowerCase() ==
                element.Parameter_id.toLowerCase() &&
              this.dataThres.Criticality.toLowerCase() ==
                element.Criticality.toLowerCase() &&
              this.dataThres.model.toLowerCase() ==
                element.model.toLowerCase() &&
              this.dataThres.priority == element.priority
            ) {
              countThres++;
            }
          });
        }
        if (!this.top && this.bottom) {
          this.dataThres.Criticality = "Caution Bottom";
          this.dataThres.Limit_Value = this.caution_bottom;
          console.log(this.dataThres);
          datas.forEach(element => {
            if (
              this.dataThres.Parameter_id.toLowerCase() ==
                element.Parameter_id.toLowerCase() &&
              this.dataThres.Criticality.toLowerCase() ==
                element.Criticality.toLowerCase() &&
              this.dataThres.model.toLowerCase() ==
                element.model.toLowerCase() &&
              this.dataThres.priority == element.priority
            ) {
              countThres++;
            }
          });
        }

        if (this.top && this.bottom) {
          this.dataThres.Criticality = "Caution Top";
          this.dataThres.Limit_Value = this.caution_bottom;
          console.log(this.dataThres);
          datas.forEach(element => {
            if (
              this.dataThres.Parameter_id.toLowerCase() ==
                element.Parameter_id.toLowerCase() &&
              this.dataThres.Criticality.toLowerCase() ==
                element.Criticality.toLowerCase() &&
              this.dataThres.model.toLowerCase() ==
                element.model.toLowerCase() &&
              this.dataThres.priority == element.priority
            ) {
              countThres_top++;
            }
          });

          this.dataThres.Criticality = "Caution Bottom";
          this.dataThres.Limit_Value = this.caution_bottom;
          console.log(this.dataThres);
          datas.forEach(element => {
            if (
              this.dataThres.Parameter_id.toLowerCase() ==
                element.Parameter_id.toLowerCase() &&
              this.dataThres.Criticality.toLowerCase() ==
                element.Criticality.toLowerCase() &&
              this.dataThres.model.toLowerCase() ==
                element.model.toLowerCase() &&
              this.dataThres.priority == element.priority
            ) {
              countThres_bot++;
            }
          });
        }

        if (
          countThres_top > 0 ||
          countThres_bot > 0 ||
          countParam > 0 ||
          countThres > 0 ||
          this.selectedAnalysis.length == 0 ||
          this.selectedRecommendation.length == 0
        ) {
          if (countParam > 0) {
            this.paramAvail = true;
          }
          if (countThres > 0) {
            this.anAvail = true;
          }

          if (countThres_top > 0) {
            this.countThresTop = true;
          }

          if (countThres_bot > 0) {
            this.countThresBot = true;
          }

          if (this.selectedAnalysis.length == 0) {
            this.analysisNotSelected = true;
          }

          if (this.selectedRecommendation.length == 0) {
            this.recommendationNotSelected = true;
          }

          setTimeout(() => {
            this.paramAvail = false;
            this.countThresTop = false;
            this.countThresBot = false;
            this.anAvail = false;
            this.analysisNotSelected = false;
            this.recommendationNotSelected = false;
          }, 10000);

          this.submitted = false;
        } else {
          console.log('berhasil');
          this.paramService.postPara(this.dataParameter).subscribe(datau => {
            console.log('post parameter');
            console.log(datau);
          });

          if (this.top && !this.bottom) {
            this.dataThres.Criticality = "Caution Top";
            this.dataThres.Limit_Value = this.caution_top;
            console.log("data para:" + JSON.stringify(this.dataThres));
            this.pcarService.postThres(this.dataThres).subscribe(data => {
                console.log(data);
                this.dataThres.Criticality = "Critical Top";
                this.dataThres.Limit_Value = this.critical_top;
                this.pcarService.postThres(this.dataThres).subscribe(datas => {
                  console.log(datas);
                })
            });
          }

          if (!this.top && this.bottom) {
            this.dataThres.Criticality = "Caution Bottom";
            this.dataThres.Limit_Value = this.caution_bottom;
            console.log("data para:" + JSON.stringify(this.dataThres));
            this.pcarService
              .postThres(this.dataThres)
              .subscribe(data => {
                console.log(data);
                this.dataThres.Criticality = "Critical Bottom";
                this.dataThres.Limit_Value = this.critical_bottom;
                this.pcarService
                  .postThres(this.dataThres)
                  .subscribe(datas => {
                    console.log(datas);
                  });
              });
          }

          if (this.top && this.bottom) {
            this.dataThres.Criticality = "Caution Top";
            this.dataThres.Limit_Value = this.caution_top;
            console.log("data para:" + JSON.stringify(this.dataThres));
            this.pcarService.postThres(this.dataThres).subscribe(data => {
                console.log(data);
                this.dataThres.Criticality = "Critical Top";
                this.dataThres.Limit_Value = this.critical_top;
                this.pcarService.postThres(this.dataThres).subscribe(datax => {
                    console.log(datax);
                    this.dataThres.Criticality = "Caution Bottom";
                    this.dataThres.Limit_Value = this.caution_bottom;
                    this.pcarService.postThres(this.dataThres).subscribe(datay => {
                      console.log(datay);
                       this.dataThres.Criticality = "Critical Bottom";
                       this.dataThres.Limit_Value = this.critical_bottom;
                       this.pcarService.postThres(this.dataThres).subscribe(dataz => {
                         console.log(dataz);
                       });
                    });
                  });
              });
          }

          /* console.log("data para:" + JSON.stringify(this.dataThres));
          this.pcarService.postThres(this.dataThres).subscribe(data => {
            console.log(data);
          }); */

          for (var i = 0; i < this.selectedAnalysis.length; i++) {
            console.log("1" + JSON.stringify(this.selectedAnalysis[i]));
            this.dataAnalysis.Solving_id = this.newIdSolving;
            this.dataAnalysis.Threshold_id = this.newIdThreshold;
            this.dataAnalysis.Analysis_id = this.selectedAnalysis[i];
            console.log(this.dataAnalysis);
            this.solvingAnService
              .postPara(this.dataAnalysis)
              .subscribe(data2 => {
                console.log("2 " + JSON.stringify(data2));
              });
          }

          for (var i = 0; i < this.selectedRecommendation.length; i++) {
            console.log("1" + JSON.stringify(this.selectedRecommendation[i]));
            this.dataRec.Solving_id = this.newIdSolving;
            this.dataRec.Threshold_id = this.newIdThreshold;
            this.dataRec.Recommendation_id = this.selectedRecommendation[i];
            console.log(this.dataRec);
            this.solvingRecService.postPara(this.dataRec).subscribe(data3 => {
              console.log("2 " + JSON.stringify(data3));
            });
          }

          setTimeout(function() {
            alert('Data Berhasil Disimpan');
            window.location.reload();
          }, 2000);
        }
      });
    });
    /* console.log(this.dataThres); */
    /* this.pcarService.getThres().subscribe(data => {
      let count = 0;
      console.log("data threshold master:" + JSON.stringify(data));
      data.forEach(element => {
        if (
          this.dataThres.Parameter_id.toLowerCase() ==
            element.Parameter_id.toLowerCase() &&
          this.dataThres.Criticality.toLowerCase() ==
            element.Criticality.toLowerCase() &&
          this.dataThres.Limit_Value == element.Limit_Value &&
          this.dataThres.model.toLowerCase() == element.model.toLowerCase() &&
          this.dataThres.priority == element.priority
        ) {
          count++;
        }
      });

      if (
        count > 0 ||
        this.selectedAnalysis.length == 0 ||
        this.selectedRecommendation.length == 0
      ) {
        if (count > 0) {
          this.anAvail = true;
        }

        if (this.selectedAnalysis.length == 0) {
          this.analysisNotSelected = true;
        }

        if (this.selectedRecommendation.length == 0) {
          this.recommendationNotSelected = true;
        }

        setTimeout(() => {
          this.anAvail = false;
          this.analysisNotSelected = false;
          this.recommendationNotSelected = false;
        }, 10000);

        this.submitted = false;
      } else {
        console.log("data para:" + JSON.stringify(this.dataThres));
        this.pcarService.postThres(this.dataThres).subscribe(data => {
          console.log(data);
        });

        for (var i = 0; i < this.selectedAnalysis.length; i++) {
          console.log("1" + JSON.stringify(this.selectedAnalysis[i]));
          this.dataAnalysis.Solving_id = this.newIdSolving;
          this.dataAnalysis.Threshold_id = this.newIdThreshold;
          this.dataAnalysis.Analysis_id = this.selectedAnalysis[i];
          console.log(this.dataAnalysis);
          this.solvingAnService.postPara(this.dataAnalysis).subscribe(data2 => {
            console.log("2 " + JSON.stringify(data2));
          });
        }

        for (var i = 0; i < this.selectedRecommendation.length; i++) {
          console.log("1" + JSON.stringify(this.selectedRecommendation[i]));
          this.dataRec.Solving_id = this.newIdSolving;
          this.dataRec.Threshold_id = this.newIdThreshold;
          this.dataRec.Recommendation_id = this.selectedRecommendation[i];
          console.log(this.dataRec);
          this.solvingRecService.postPara(this.dataRec).subscribe(data3 => {
            console.log("2 " + JSON.stringify(data3));
          });
        }

        setTimeout(function() {
          window.location.reload();
        }, 2000);
      }
    }); */
  }
}

