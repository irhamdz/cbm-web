import { RecommendationComponent } from './recommendation.component';
import { RecommendationService } from './recommendation.service';
import {NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';


@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule
    ],
    declarations: [RecommendationComponent],
    providers: [RecommendationService],
    bootstrap: [RecommendationComponent],
})
export class RecomModule{
}