import { PcarService } from './../service/pcar.service';
import { Component, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ParameterService } from './parameter-master.service';
import { Observable, Subject } from 'rxjs/Rx';
import { DataTableDirective } from 'angular-datatables'; 


@Component({
  selector: "app-post",
  templateUrl: "parameter-master.component.html",
  providers: [ParameterService]
})
export class ParameterMasterComponent {
  @ViewChild(DataTableDirective) datatableElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<String> = new Subject();

  idParameter = "";
  contents: any[];
  trendData: any[];
  trendDataname:any[];
  parameterTrend: any[];
  pager: any = {};
  pagedItems: any[];
  constructor(
    private paraServ: ParameterService,
    private pcarServ: PcarService,
    public router: Router
  ) {}

  private ngOnInit() {
    this.paraServ.getPara().subscribe(data => {
      console.log(data);
      this.contents = data;
      console.log('DATA PARAMETER : ' + JSON.stringify(this.contents))
      this.dtTrigger.next();
      /* this.setPage(1); */
    });

    this.pcarServ.getAllSourceTrend().subscribe(data => {
      console.log(data);
      this.trendData = data;
    });
    this.pcarServ.getAllTrendData().subscribe(data => {
      console.log(data);
      this.trendDataname = data;
    });

    this.paraServ.getMaxManyId().subscribe(data => {
      data.forEach(element => {
        console.log("data max id:" + JSON.stringify(element.max_param));
        let stringAboutChange = element.max_param.slice(3);
        stringAboutChange = +stringAboutChange + 1;

        let lengthStr = element.max_param.length; //length of str from max_param
        /* let lenghtNum = stringAboutChange.toString().length */
        let maxIdParam = element.max_param.slice(0, 3);

        let lengthOfZero = lengthStr - maxIdParam.length;
        lengthOfZero = stringAboutChange.toString().padStart(lengthOfZero, "0"); //add '0' basicly to length of str - maxIdParam.length
        this.idParameter = maxIdParam + lengthOfZero;
        console.log("new data" + JSON.stringify(this.idParameter));
      });
    });
  }
  submitted = false;
  dataPara = {
    Parameter_id: "",
    trend_id: '',
    Component: "",
    combination: "",
    Parameter: "",
    Model: "PC2000-8",
    Priority: 0,
    source: ""
  };
  //add data
  anAvail = null;
  addNewAnalysis() {
    this.submitted = true;

    let Parameter_id = this.idParameter;
    /* Parameter_id = Parameter_id.charAt(0).toUpperCase() + Parameter_id.slice(1); */
    this.dataPara.Parameter_id = Parameter_id;

    let Component = this.dataPara.Component;
    Component = Component.toLowerCase();
    Component = Component.charAt(0).toUpperCase() + Component.slice(1);
    this.dataPara.Component = Component;

    let Parameter = this.dataPara.Parameter;
    this.dataPara.Parameter = this.an.trend_name;

    let id = this.dataPara.trend_id;
    this.dataPara.trend_id = this.an.id;

    let combination = this.dataPara.combination;
    this.dataPara.combination = combination;

    console.log("data para:" + JSON.stringify(this.dataPara));

    this.paraServ.getPara().subscribe(data => {
      let count = 0;
      console.log('data parameter master:' + JSON.stringify(data));
      data.forEach(element => {
        console.log('data element:' + JSON.stringify(element.source));
        if (
          this.dataPara.source === element.source &&
          this.dataPara.Parameter === element.Parameter &&
          this.dataPara.Component.toLowerCase() === element.Component.toLowerCase()
        ) {
          count++;
        }
      });

      if (count > 0) {
        this.anAvail = true;
        setTimeout(() => {
          this.anAvail = false;
        }, 5000);
        this.submitted = false;
      } else {
        console.log("data para:" + JSON.stringify(this.dataPara));
        this.paraServ.postPara(this.dataPara).subscribe(data => {
          alert("Your data has been added in database");
        }, window.location.reload());
      }
    });

    /*  this.paraServ.getMaxManyId().subscribe(data =>{
      data.forEach(element => {
        console.log('data max id:' + JSON.stringify(element.max_param));
        let stringAboutChange = element.max_param.slice(3)
        stringAboutChange = +stringAboutChange + 1;

        let lengthStr = element.max_param.length //length of str from max_param
        let lenghtNum = stringAboutChange.toString().length
        let maxIdParam = element.max_param.slice(0, 3);

        let lengthOfZero = lengthStr - (maxIdParam.length);
        lengthOfZero = stringAboutChange.toString().padStart(lengthOfZero, "0"); //add '0' basicly to length of str - maxIdParam.length
        this.dataPara.Parameter_id = maxIdParam + lengthOfZero;
        console.log('new data' + JSON.stringify(this.dataPara));
      })
    }); */

    /*  let Parameter_id = this.idParameter;
    Parameter_id = Parameter_id.charAt(0).toUpperCase() + Parameter_id.slice(1);
    this.dataPara.Parameter_id = Parameter_id;

    let Parameter = this.dataPara.Parameter;
    Parameter = Parameter.charAt(0).toUpperCase() + Parameter.slice(1);
    this.dataPara.Parameter = Parameter;

    let Model = this.dataPara.Model;
    Model = Model.charAt(0).toUpperCase() + Model.slice(1);
    this.dataPara.Model = Model;

    let Component = this.dataPara.Component;
    Component = Component.charAt(0).toUpperCase() + Component.slice(1);
    this.dataPara.Component = Component; */

    /* this.paraServ.getPara().subscribe(data => {
      let count = 0;
      data.forEach(element => {
        if (
          Parameter_id.toLowerCase() === element.Parameter_id.toLowerCase() &&
          Parameter.toLowerCase() === element.Parameter.toLowerCase() &&
          Model.toLowerCase() === element.Model.toLowerCase() &&
          Component.toLowerCase() === element.Component.toLowerCase()
        ) {
          count++;
        }
      });

      if (count > 0) {
        this.anAvail = true;
        setTimeout(() => {
          this.anAvail = false;
        }, 5000);

        this.submitted = false;
      } else {
        console.log("data para:" + JSON.stringify(this.dataPara));
        this.paraServ.postPara(this.dataPara).subscribe(data => {
          alert("Your data has been added in database");
        }, window.location.reload());
      }
    }); */
  }

  // function delete
  contentTitleWillBeDeleted = "";
  dataContentToBeDeleted = { content: "", item: "" };
  willDeleteContent(content, item) {
    this.contentTitleWillBeDeleted = item.Parameter;
    this.dataContentToBeDeleted.content = content;
    this.dataContentToBeDeleted.item = item;
  }
  deleteContent() {
    this.paraServ
      .deletePara(this.dataContentToBeDeleted.item["Parameter_id"])
      .subscribe(data => {
        console.log("Data yang akan dihapus" + JSON.stringify(this.dataContentToBeDeleted.item["Parameter_id"]))
        alert("Your data has been deleted from database");
      }, window.location.reload());
  }

  // function edit
  showEditContent = false;
  para: any;
  cancelEdit() {
    this.showEditContent = false;
  }
  willEditContent(content, item) {
    console.log(content, item);
    this.showEditContent = true;
    this.para = item;
  }
  editContent() {
    this.submitted = true;

    let Parameter = this.para.Parameter;
    this.para.Parameter = this.an2.trend_name;

    let Component = this.para.Component;
    Component = Component.charAt(0).toUpperCase() + Component.slice(1);
    this.para.Component = Component;

    let combination =this.para.combination;
    this.para.combination = combination;

    let id = this.para.trend_id;
    this.para.trend_id = this.an2.id;

    console.log("data para:" + JSON.stringify(this.para));

    this.paraServ.getPara().subscribe(data => {
      let count = 0;
      console.log('data parameter master:' + JSON.stringify(data));
      data.forEach(element => {
        console.log('data element:' + JSON.stringify(element.source));
        if (
          this.para.source === element.source &&
          this.para.Parameter === element.Parameter &&
          this.para.Component.toLowerCase() === element.Component.toLowerCase()
        ) {
          count++;
        }
      })


      if (count > 0) {
        this.anAvail = true;
        setTimeout(() => {
          this.anAvail = false;
        }, 5000);

        this.submitted = false;
      } else {
        this.paraServ.putPara(this.para).subscribe(data => {
          alert("Your data has been updated in database");
        });
      }
    });
  }

  onChangeSource(index, item) {
    /* let source = item.trend_source;
    this.an.id = item.id; */
    console.log(item.trend_source);
    this.pcarServ.getParameterBySource(item.trend_source).subscribe(datas => {
      console.log("data parameter: " + JSON.stringify(datas));
      /* this.parameterTrend = this.parameterTrend; */
      this.parameterTrend = datas;
      this.dataPara.source = item.trend_source;
    });
  }

  an: any;
  onChangeParameter(index, item) {
    this.an = item;

    let id = item.id;
    let name = item.name;

    this.an.id = item.id;
    this.an.trend_name = item.trend_name;
    console.log(this.an);
 
}

  onChangeSourceEdit(index, item) {
    /* let source = item.trend_source;
    this.an.id = item.id; */
    console.log(item.trend_source);
    this.pcarServ.getParameterBySource(item.trend_source).subscribe(datas => {
      console.log("data parameter: " + JSON.stringify(datas));
      /* this.parameterTrend = this.parameterTrend; */
      this.parameterTrend = datas;
      this.para.source = item.trend_source;
    });
  }

  an2: any;
  onChangeParameterEdit(index, item) {
    this.an2 = item;

    let id = item.id;
    let name = item.name;

    this.an2.id = item.id;
    this.an2.trend_name = item.trend_name;
    console.log(this.an2);

  }
  /* setPage(page: number); {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    // get pager object from service
    this.pager = this.paraServ.getPager(this.contents.length, page);

    // get current page of items
    this.pagedItems = this.contents.slice(this.pager.startIndex, this.pager.endIndex + 1);
  } */
}
