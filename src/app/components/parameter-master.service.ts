import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';

@Injectable()
export class ParameterService {
  title = "app works!";
  apiUrlPara = /* "http://cbm-api.unitedtractors.com/api/parameter_master_tables" */"http://cbm-api.unitedtractors.com/api/parameter_master_tables/";
  apiUrlMaxId = "http://cbm-api.unitedtractors.com/api/max_many_masters/";

  constructor(public http: Http) {
    console.log("Hello fellow user");
  }

  getMaxManyId() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.apiUrlMaxId)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  getPara() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.apiUrlPara)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  deletePara(Parameter_id) {
    return Observable.create(observer => {
      return this.http
        .delete(this.apiUrlPara +  Parameter_id)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }
  putPara(para) {
    return Observable.create(observer => {
      return this.http
        .patch(this.apiUrlPara + para.Parameter_id, para)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }
  postPara(dataPara) {
    return Observable.create(observer => {
      return this.http
        .post(this.apiUrlPara, dataPara)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }
  getPager(totalItems: number, currentPage: number = 1, pageSize: number = 10) {
    // calculate total pages
    let totalPages = Math.ceil(totalItems / pageSize);

    let startPage: number, endPage: number;
    if (totalPages <= 10) {
      // less than 10 total pages so show all
      startPage = 1;
      endPage = totalPages;
    } else {
      // more than 10 total pages so calculate start and end pages
      if (currentPage <= 6) {
        startPage = 1;
        endPage = 10;
      } else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
      } else {
        startPage = currentPage - 5;
        endPage = currentPage + 4;
      }
    }

    // calculate start and end item indexes
    let startIndex = (currentPage - 1) * pageSize;
    let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

    // create an array of pages to ng-repeat in the pager control
    let pages = _.range(startPage, endPage + 1);

    // return object with all pager properties required by the view
    return {
      totalItems: totalItems,
      currentPage: currentPage,
      pageSize: pageSize,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pages
    };
  }
}