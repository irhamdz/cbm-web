import { ParameterService } from './parameter-master.service';
import { UnitConversionService } from './../service/unit-conversion.service';
import { SolvingRecomServ } from './../service/solving-recom.service';
import { SolvingAnalysisServ } from './../service/solving-analysis.service';
import { UnitConversionComponent } from './unit-conversion.component';
import { UploadService } from './../service/upload.service';
import { UploadComponent } from './upload.component';
import { PcarListService } from './../service/pcar-list.service';
import { PcarService } from './../service/pcar.service';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { ButtonsComponent } from './buttons.component';
import { CardsComponent } from './cards.component';
import { FormsComponent } from './forms.component';
import { SocialButtonsComponent } from './social-buttons.component';
import { SwitchesComponent } from './switches.component';
import { TablesComponent } from './tables.component';
/* import { DataTablesModule } from 'angular-datatables'; */

// Modal Components
import { ModalModule } from 'ng2-bootstrap/modal';
import { ModalsComponent } from './modals.component';

// Tabs Component
import { TabsModule } from 'ng2-bootstrap/tabs';
import { TabsComponent } from './tabs.component';

// Components Routing
import { ComponentsRoutingModule } from './components-routing.module';

import { HomeComponent } from './home.component';
import { GeneralComponent } from './general.component';
import { HealthScoreComponent } from './health-score.component';
import { KPIComponent } from './kpi.component';
import { AlertComponent } from './alert.component';
import { PCARComponent } from './pcar.component';
import { ThresholdComponent } from './threshold.component';
import { AnalysisComponent } from './analysis-master.component';
import { RecommendationComponent } from './recommendation.component';
import { DataRequirementComponent } from './data-requirement.component';
import { ParameterMasterComponent } from './parameter-master.component';
import { SolvingMasterComponent } from './solving-master.component';
import { UserManagementComponent } from './user-management.component';
import { pcarlistComponent } from './pcarlist.component';


// new
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

//datatables
 import { DataTablesModule } from 'angular-datatables'; 
 import { SolvingAnalysisComponent } from 'app/components/solving-analysis.component';
 import { SolvingRecommendationComponent } from 'app/components/solving-recommendation.component';

 //login
  import { LoginComponent } from 'app/pages/login.component';

    //multiselect
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';


@NgModule({
  imports: [
    HttpModule,
    ComponentsRoutingModule,
    ModalModule.forRoot(),
    DataTablesModule.forRoot(),
    TabsModule,
    ChartsModule,
    CommonModule,
    FormsModule,
    AngularMultiSelectModule
  ],
  declarations: [
    ButtonsComponent,
    CardsComponent,
    FormsComponent,
    ModalsComponent,
    SocialButtonsComponent,
    SwitchesComponent,
    TablesComponent,
    TabsComponent,

    HomeComponent,
    GeneralComponent,
    HealthScoreComponent,
    KPIComponent,
    AlertComponent,
    PCARComponent,
    ThresholdComponent,
    RecommendationComponent,
    UserManagementComponent,
    AnalysisComponent,
    DataRequirementComponent,
    ParameterMasterComponent,
    SolvingMasterComponent,
    pcarlistComponent,
    UploadComponent,
    SolvingAnalysisComponent,
    SolvingRecommendationComponent,
    UnitConversionComponent,
    LoginComponent
  ],
  providers: [
    PcarService,
    PcarListService,
    UploadService,
    SolvingAnalysisServ,
    SolvingRecomServ,
    UnitConversionService,
    ParameterService]
})
export class ComponentsModule { }
