import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';

@Injectable()
export class AnalysisService {
  title = "app works!";
  apiUrlAnalysis = "http://cbm-api.unitedtractors.com/api/analysis_master_tables/";
  apiUrlMaxId = "http://cbm-api.unitedtractors.com/api/max_many_masters/";
  constructor(public http: Http) {
    console.log("Hello fellow user");
  }
  deleteAnalysis(Analysis_id) {
    return Observable.create(observer => {
      return this.http
        .delete(this.apiUrlAnalysis + Analysis_id)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }
  putAnalisis(analisis) {
    return Observable.create(observer => {
      return this.http
        .patch(this.apiUrlAnalysis + analisis.Analysis_id, analisis)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }
  postAnalysis(dataAn) {
    return Observable.create(observer => {
      return this.http
        .post(this.apiUrlAnalysis, dataAn)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }
  getAllAnalysis() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.apiUrlAnalysis)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }
  getMaxManyId() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.apiUrlMaxId)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }
}