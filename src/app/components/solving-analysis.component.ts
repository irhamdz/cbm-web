import { Component, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SolvingAnalysisServ } from '../service/solving-analysis.service';
import { Observable, Subject } from 'rxjs/Rx';
import { DataTableDirective } from 'angular-datatables'; 

@Component({
  selector: "app-post",
  templateUrl: 'solving-analysis.component.html',
  providers: [SolvingAnalysisServ]
})
export class SolvingAnalysisComponent {
  @ViewChild(DataTableDirective) datatableElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<String> = new Subject();

  constructor(
    private solan: SolvingAnalysisServ,
    public router: Router) { }

  contents: any[];
  idSolv = "";
  thresData: any[];
  analysisData: any[];
  private ngOnInit() {
    this.solan.getPara().subscribe(data => {
      console.log(data);
      this.contents = data;
      this.dtTrigger.next();
      /* this.setPage(1); */
    });
    this.solan.getMaxManyId().subscribe(data => {
      data.forEach(element => {
        console.log("data max id:" + JSON.stringify(element.max_solv_analysis));
        let stringAboutChange = element.max_solv_analysis;
        stringAboutChange = +stringAboutChange + 1;

        this.idSolv = stringAboutChange;
        console.log("new data" + JSON.stringify(this.idSolv));
      });
    });
    this.solan.getThres().subscribe(data => {
      console.log(data);
      this.thresData = data;
    });
    this.solan.getAnalysis().subscribe(data => {
      console.log(data);
      this.analysisData = data;
    });
  }

  submitted = false;
  anAvail=null;
  dataSolv= {
    Solving_id : "",
    Threshold_id : "",
    Analysis_id: ""
  };
  an: any;
  onChangeThres(index, item) {
    this.an = item;

    let Threshold_id = item.Threshold_id;

    this.an.Threshold_id = item.Threshold_id;
    console.log(this.an);

  }
  an2: any;
  onChangeAnalysis(index, item) {
    this.an2 = item;

    let Analysis_id = item.Analysis_id;
    let Analysis = item.Analysis;

    this.an2.Analysis_id = item.Analysis_id;
    this.an2.Analysis = item.Analysis;
    console.log(this.an2);

  }
  an3: any;
  onChangeThresEdit(index, item) {
    this.an3 = item;

    let Threshold_id = item.Threshold_id;

    this.an3.Threshold_id = item.Threshold_id;
    console.log(this.an3);

  }
  an4: any;
  onChangeAnalysisEdit(index, item) {
    this.an4 = item;

    let Analysis_id = item.Analysis_id;
    let Analysis = item.Analysis;

    this.an4.Analysis_id = item.Analysis_id;
    this.an4.Analysis = item.Analysis;
    console.log(this.an4);

  }
  addNewSolv() {
    this.submitted = true;

    let Solving_id = this.idSolv;
    /* Parameter_id = Parameter_id.charAt(0).toUpperCase() + Parameter_id.slice(1); */
    this.dataSolv.Solving_id = Solving_id;

    let Threshold_id = this.dataSolv.Threshold_id;
    this.dataSolv.Threshold_id = this.an.Threshold_id;

    let Analysis_id = this.dataSolv.Analysis_id;
    this.dataSolv.Analysis_id = this.an2.Analysis_id;


    console.log("data para:" + JSON.stringify(this.dataSolv));

    this.solan.getPara().subscribe(data => {
      let count = 0;
      console.log('data solving analysis:' + JSON.stringify(data));


      if (count > 0) {
        this.anAvail = true;
        setTimeout(() => {
          this.anAvail = false;
        }, 5000);
        this.submitted = false;
      } else {
        console.log("data para:" + JSON.stringify(this.dataSolv));
        this.solan.postPara(this.dataSolv).subscribe(data => {
          alert("Your data has been added in database");
        }, window.location.reload());
      }
    });
  }

  // function delete
  contentTitleWillBeDeleted = "";
  dataContentToBeDeleted = { content: "", item: "" };
  willDeleteContent(content, item) {
    this.contentTitleWillBeDeleted = item.Solving_id;
    this.dataContentToBeDeleted.content = content;
    this.dataContentToBeDeleted.item = item;
  }
  deleteContent() {
    this.solan
      .deletePara(this.dataContentToBeDeleted.item["Solving_id"])
      .subscribe(data => {
        console.log("Data yang akan dihapus" + JSON.stringify(this.dataContentToBeDeleted.item["Solving_id"]))
        alert("Your data has been deleted from database");
      }, window.location.reload());
  }

  // function edit
  showEditContent = false;
  para: any;
  cancelEdit() {
    this.showEditContent = false;
  }
  willEditContent(content, item) {
    console.log(content, item);
    this.showEditContent = true;
    this.para = item;
  }
  editContent() {
    this.submitted = true;

    let Threshold_id = this.para.Threshold_id;
    this.para.Threshold_id = this.an3.Threshold_id;

    let Analysis_id = this.para.Analysis_id;
    this.para.Analysis_id = this.an4.Analysis_id;

    console.log("data para:" + JSON.stringify(this.para));

    this.solan.getPara().subscribe(data => {
      let count = 0;


      if (count > 0) {
        this.anAvail = true;
        setTimeout(() => {
          this.anAvail = false;
        }, 5000);

        this.submitted = false;
      } else {
        this.solan.putPara(this.para).subscribe(data => {
          alert("Your data has been updated in database");
        }, window.location.reload());
      }
    });
  }
}
