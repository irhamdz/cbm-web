import { AnalysisComponent} from './analysis-master.component';
import {AnalysisService} from './analysis-master.service';
import {NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule
    ],
    declarations: [AnalysisComponent],
    providers: [AnalysisService],
    bootstrap: [AnalysisComponent]
})
export class AnalysisModule{}