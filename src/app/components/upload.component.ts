import { UploadService } from './../service/upload.service';
import { ChangeDetectorRef, Component } from "@angular/core";

@Component({
  selector: "folder-upload",
  templateUrl: "upload.component.html"
})
export class UploadComponent {
  /* buildTree: any;
  canDropFolder = typeof DataTransferItem.prototype.webkitGetAsEntry ===
    'function'; */
  uploadPaths = [];
  upload = [];
  path = [];
  lastDate: number;
  fileReaded: any;

  dataUpload = {
    MACHINE_SERIAL_NO: "",
    TIME_STAMP: "",
    SMR: 0,
    SCALE_ENGSPEED_MAX_: "",
    SCALE_ENGSPEED_AVE_: "",
    SCALE_BLOWBYPRESS_MAX: "",
    SCALE_LBF_EXHTEMPMAX: "",
    SCALE_LBR_EXHTEMPMAX: "",
    SCALE_RBF_EXHTEMPMAX: "",
    SCALE_RBR_EXHTEMPMAX: "",
    SCALE_BOOST_PRESS_MAX: "",
    SCALE_EOIL_PREMAX: "",
    SCALE_EOIL_PL_MIN: "",
    SCALE_EOIL_PH_MIN: "",
    SCALE_ENGOIL_TMPMAX: "",
    SCALE_COOL_TEMPMAX: "",
    SCALE_COOL_TEMPMIN: "",
    SCALE_FUEL_RATE: "",
    SCALE_AMBIENT_TEMPMAX: "",
    SCALE_AMBIENT_TEMPAVE: "",
    SCALE_AMBIENT_TEMPMIN: "",
    SCALE_ATOMOS_PRESAVE: "",
    SCALE_PUMP_1F_PMAX: "",
    SCALE_PUMP_1R_PMAX: "",
    SCALE_PUMP_2F_PMAX: "",
    SCALE_PUMP_2R_PMAX: "",
    SCALE_FANPUMPF_PMAX: "",
    SCALE_FANPUMPR_PMAX: "",
    SCALE_HYDOILTEMPMAX: "",
    SCALE_HYDOILTEMPAVE: "",
    SCALE_HYDOILTEMPMIN: "",
    SCALE_PTO_TEMP_MAX: "",
    SCALE_PTO_TEMP_MIN: "",
    SCALE_TRUCK_COUNTER1: "",
    SCALE_TRUCK_COUNTER2: "",
    SCALE_LOAD1F: "",
    SCALE_LOAD1R: "",
    SCALE_LOAD2F: "",
    SCALE_LOAD2R: "",
    SCALE_LOADFF: "",
    SCALE_LOADFR: "",
    SCALE_LOAD_COUNT: "",
    SCALE_SWING_COUNT: "",
    SCALE_AUTO_GRS_PMAX: "",
    SCALE_AUTO_GRS_P_ON: "",
    SCALE_PUMP1_TORQUE_MAX: "",
    SCALE_PUMP1_TORQUE_AVE: "",
    SCALE_PUMP2_TORQUE_MAX: "",
    SCALE_PUMP2_TORQUE_AVE: "",
    SCALE_FANPUMPTORQUE_MAX: "",
    SCALE_FANPUMPTORQUE_AVE: "",
    SCALE_ENGINE_POWER_MAX: "",
    SCALE_ENGINE_POWER_AVE: "",
    SCALE_ECO_MODE_ON: "",
    SMR_2: 0,
    CALENDAR_SATELITE: "",
    ENGSPEED_MAX_: 0,
    ENGSPEED_AVE_: 0,
    BLOWBYPRESS_MAX: 0,
    LBF_EXHTEMPMAX: 0,
    LBR_EXHTEMPMAX: 0,
    RBF_EXHTEMPMAX: 0,
    RBR_EXHTEMPMAX: 0,
    BOOST_PRESS_MAX: 0,
    EOIL_PREMAX: 0,
    EOIL_PL_MIN: 0,
    EOIL_PH_MIN: 0,
    ENGOIL_TMPMAX: 0,
    COOL_TEMPMAX: 0,
    COOL_TEMPMIN: 0,
    FUEL_RATE: 0,
    AMBIENT_TEMPMAX: 0,
    AMBIENT_TEMPAVE: 0,
    AMBIENT_TEMPMIN: 0,
    ATOMOS_PRESAVE: 0,
    PUMP_1F_PMAX: 0,
    PUMP_1R_PMAX: 0,
    PUMP_2F_PMAX: 0,
    PUMP_2R_PMAX: 0,
    FANPUMPF_PMAX: 0,
    FANPUMPR_PMAX: 0,
    HYDOILTEMPMAX: 0,
    HYDOILTEMPAVE: 0,
    HYDOILTEMPMIN: 0,
    PTO_TEMP_MAX: 0,
    PTO_TEMP_MIN: 0,
    TRUCK_COUNTER1: 0,
    TRUCK_COUNTER2: 0,
    LOAD1F: 0,
    LOAD1R: 0,
    LOAD2F: 0,
    LOAD2R: 0,
    LOADFF: 0,
    LOADFR: 0,
    LOAD_COUNT: 0,
    SWING_COUNT: 0,
    AUTO_GRS_PMAX: 0,
    AUTO_GRS_P_ON: 0,
    PUMP1_TORQUE_MAX: 0,
    PUMP1_TORQUE_AVE: 0,
    PUMP2_TORQUE_MAX: 0,
    PUMP2_TORQUE_AVE: 0,
    FANPUMPTORQUE_MAX: 0,
    FANPUMPTORQUE_AVE: 0,
    ENGINE_POWER_MAX: 0,
    ENGINE_POWER_AVE: 0,
    ECO_MODE_ON: 0
  };

  constructor(private cdr: ChangeDetectorRef, private uploadService: UploadService) {}

  filesPicked(files) {
    console.log(files);
    this.uploadPaths = [];
    Array.prototype.forEach.call(files, element => {
      const path = element.webkitRelativePath.split("/");
      const path_one_number = +path[1];
      const max_path_one = Math.max(path_one_number);
      this.path.push(path_one_number);
    });
    this.lastDate = Math.max(...this.path);
    console.log(this.lastDate);
    for (let index = 0; index < files.length; index++) {
      const element = files[index];
      const path = element.webkitRelativePath.split("/");
      if (path[1] === this.lastDate.toString()) {
        if (path[3] === "trend0_1.CSV") {
          this.uploadPaths.push(element.webkitRelativePath);
          this.upload.push(element);
        }
      }
    }
    console.log(this.upload[0]);
    /* Array.prototype.forEach.call(files, file => {
      this.uploadPaths.push(file.name);
    }); */
    this.fileReaded = this.upload[0];

    let reader: FileReader = new FileReader();
    reader.readAsText(this.fileReaded);

    reader.onload = e => {
      const csv: string = reader.result;
      let allTextLines = csv.split(/\r|\n|\r/);
      console.log(allTextLines);

      // get machine serial no
      const machine_serial_split = allTextLines[8].split(",");
      this.dataUpload.MACHINE_SERIAL_NO = machine_serial_split[1];

      // get timestamp
      const timestamp_split = allTextLines[12].split(",");
      const timestamp_slash = timestamp_split[1].split('/');
      const year = timestamp_slash[2].slice(0,4);
      const timestamp_2 = timestamp_slash[2].slice(5);
      console.log(timestamp_slash);
      console.log(year);
      console.log(timestamp_2);
      this.dataUpload.TIME_STAMP = year + '-' + timestamp_slash[1] + '-' + timestamp_slash[0] + 'T' + timestamp_2 + 'Z';
      // get SMR
      const smr_split = allTextLines[14].split(",");
      this.dataUpload.SMR = +smr_split[1];

      // get UOM
      const UOM_split = allTextLines[50].split(",");
      console.log(UOM_split);
      this.dataUpload.SCALE_ENGSPEED_MAX_ = UOM_split[3];
      this.dataUpload.SCALE_ENGSPEED_AVE_ = UOM_split[4];
      this.dataUpload.SCALE_BLOWBYPRESS_MAX = UOM_split[5];
      this.dataUpload.SCALE_LBF_EXHTEMPMAX = UOM_split[6];
      this.dataUpload.SCALE_LBR_EXHTEMPMAX = UOM_split[7];
      this.dataUpload.SCALE_RBF_EXHTEMPMAX = UOM_split[8];
      this.dataUpload.SCALE_RBR_EXHTEMPMAX = UOM_split[9];
      this.dataUpload.SCALE_BOOST_PRESS_MAX = UOM_split[10];
      this.dataUpload.SCALE_EOIL_PREMAX = UOM_split[11];
      this.dataUpload.SCALE_EOIL_PL_MIN = UOM_split[12];
      this.dataUpload.SCALE_EOIL_PH_MIN = UOM_split[13];
      this.dataUpload.SCALE_ENGOIL_TMPMAX = UOM_split[14];
      this.dataUpload.SCALE_COOL_TEMPMAX = UOM_split[15];
      this.dataUpload.SCALE_COOL_TEMPMIN = UOM_split[16];
      this.dataUpload.SCALE_FUEL_RATE = UOM_split[17];
      this.dataUpload.SCALE_AMBIENT_TEMPMAX = UOM_split[18];
      this.dataUpload.SCALE_AMBIENT_TEMPAVE = UOM_split[19];
      this.dataUpload.SCALE_AMBIENT_TEMPMIN = UOM_split[20];
      this.dataUpload.SCALE_ATOMOS_PRESAVE = UOM_split[21];
      this.dataUpload.SCALE_PUMP_1F_PMAX = UOM_split[22];
      this.dataUpload.SCALE_PUMP_1R_PMAX = UOM_split[23];
      this.dataUpload.SCALE_PUMP_2F_PMAX = UOM_split[24];
      this.dataUpload.SCALE_PUMP_2R_PMAX = UOM_split[25];
      this.dataUpload.SCALE_FANPUMPF_PMAX = UOM_split[26];
      this.dataUpload.SCALE_FANPUMPR_PMAX = UOM_split[27];
      this.dataUpload.SCALE_HYDOILTEMPMAX = UOM_split[28];
      this.dataUpload.SCALE_HYDOILTEMPAVE = UOM_split[29];
      this.dataUpload.SCALE_HYDOILTEMPMIN = UOM_split[30]
      this.dataUpload.SCALE_PTO_TEMP_MAX = UOM_split[31];
      this.dataUpload.SCALE_PTO_TEMP_MIN = UOM_split[32];
      this.dataUpload.SCALE_TRUCK_COUNTER1 = UOM_split[33];
      this.dataUpload.SCALE_TRUCK_COUNTER2 = UOM_split[34];
      this.dataUpload.SCALE_LOAD1F = UOM_split[35];
      this.dataUpload.SCALE_LOAD1R = UOM_split[36];
      this.dataUpload.SCALE_LOAD2F = UOM_split[37];
      this.dataUpload.SCALE_LOAD2R = UOM_split[38];
      this.dataUpload.SCALE_LOADFF = UOM_split[39];
      this.dataUpload.SCALE_LOADFR = UOM_split[40];
      this.dataUpload.SCALE_LOAD_COUNT = UOM_split[41];
      this.dataUpload.SCALE_SWING_COUNT = UOM_split[42];
      this.dataUpload.SCALE_AUTO_GRS_PMAX = UOM_split[43];
      this.dataUpload.SCALE_AUTO_GRS_P_ON = UOM_split[44];
      this.dataUpload.SCALE_PUMP1_TORQUE_MAX = UOM_split[45];
      this.dataUpload.SCALE_PUMP1_TORQUE_AVE = UOM_split[46];
      this.dataUpload.SCALE_PUMP2_TORQUE_MAX = UOM_split[47];
      this.dataUpload.SCALE_PUMP2_TORQUE_AVE = UOM_split[48];
      this.dataUpload.SCALE_FANPUMPTORQUE_MAX = UOM_split[49];
      this.dataUpload.SCALE_FANPUMPTORQUE_AVE = UOM_split[50];
      this.dataUpload.SCALE_ENGINE_POWER_MAX = UOM_split[51];
      this.dataUpload.SCALE_ENGINE_POWER_AVE = UOM_split[52];
      this.dataUpload.SCALE_ECO_MODE_ON = UOM_split[53];

      // get last array value (SMR,Calendar,etc)
      let last_array = allTextLines[allTextLines.length - 3].split(",");
      console.log(last_array);
      this.dataUpload.SMR_2 = +last_array[1];
      this.dataUpload.CALENDAR_SATELITE = last_array[2];
      this.dataUpload.ENGSPEED_MAX_ = +last_array[3];
      this.dataUpload.ENGSPEED_AVE_ = +last_array[4];
      this.dataUpload.BLOWBYPRESS_MAX = +last_array[5];
      this.dataUpload.LBF_EXHTEMPMAX = +last_array[6];
      this.dataUpload.LBR_EXHTEMPMAX = +last_array[7];
      this.dataUpload.RBF_EXHTEMPMAX = +last_array[8];
      this.dataUpload.RBR_EXHTEMPMAX = +last_array[9];
      this.dataUpload.BOOST_PRESS_MAX = +last_array[10];
      this.dataUpload.EOIL_PREMAX = +last_array[11];
      this.dataUpload.EOIL_PL_MIN = +last_array[12];
      this.dataUpload.EOIL_PH_MIN = +last_array[13];
      this.dataUpload.ENGOIL_TMPMAX = +last_array[14];
      this.dataUpload.COOL_TEMPMAX = +last_array[15];
      this.dataUpload.COOL_TEMPMIN = +last_array[16];
      this.dataUpload.FUEL_RATE = +last_array[17];
      this.dataUpload.AMBIENT_TEMPMAX = +last_array[18];
      this.dataUpload.AMBIENT_TEMPAVE = +last_array[19];
      this.dataUpload.AMBIENT_TEMPMIN = +last_array[20];
      this.dataUpload.ATOMOS_PRESAVE = +last_array[21];
      this.dataUpload.PUMP_1F_PMAX = +last_array[22];
      this.dataUpload.PUMP_1R_PMAX = +last_array[23];
      this.dataUpload.PUMP_2F_PMAX = +last_array[24];
      this.dataUpload.PUMP_2R_PMAX = +last_array[25];
      this.dataUpload.FANPUMPF_PMAX = +last_array[26];
      this.dataUpload.FANPUMPR_PMAX = +last_array[27];
      this.dataUpload.HYDOILTEMPMAX = +last_array[28];
      this.dataUpload.HYDOILTEMPAVE = +last_array[29];
      this.dataUpload.HYDOILTEMPMIN = +last_array[30];
      this.dataUpload.PTO_TEMP_MAX = +last_array[31];
      this.dataUpload.PTO_TEMP_MIN = +last_array[32];
      this.dataUpload.TRUCK_COUNTER1 = +last_array[33];
      this.dataUpload.TRUCK_COUNTER2 = +last_array[34];
      this.dataUpload.LOAD1F = +last_array[35];
      this.dataUpload.LOAD1R = +last_array[36];
      this.dataUpload.LOAD2F = +last_array[37];
      this.dataUpload.LOAD2R = +last_array[38];
      this.dataUpload.LOADFF = +last_array[39];
      this.dataUpload.LOADFR = +last_array[40];
      this.dataUpload.LOAD_COUNT = +last_array[41];
      this.dataUpload.SWING_COUNT = +last_array[42];
      this.dataUpload.AUTO_GRS_PMAX = +last_array[43];
      this.dataUpload.AUTO_GRS_P_ON = +last_array[44];
      this.dataUpload.PUMP1_TORQUE_MAX = +last_array[45];
      this.dataUpload.PUMP1_TORQUE_AVE = +last_array[46];
      this.dataUpload.PUMP2_TORQUE_MAX = +last_array[47];
      this.dataUpload.PUMP2_TORQUE_AVE = +last_array[48];
      this.dataUpload.FANPUMPTORQUE_MAX = +last_array[49];
      this.dataUpload.FANPUMPTORQUE_AVE = +last_array[50];
      this.dataUpload.ENGINE_POWER_MAX = +last_array[51];
      this.dataUpload.ENGINE_POWER_AVE = +last_array[52];
      this.dataUpload.ECO_MODE_ON = +last_array[53];

      const headers = allTextLines[0].split(",");
      const lines = [];

      console.log(this.dataUpload);
      this.uploadService.postUpload(this.dataUpload)
        .subscribe(datas => {
          console.log(
            "response data upload: " + JSON.stringify(datas)
          );
        });

      /* this.dataUpload.MACHINE_SERIAL_NO = Parameter_id;

      let Component = this.dataPara.Component;
      Component = Component.toLowerCase();
      this.dataPara.Component = Component;

      const Combination = this.dataPara.Combination;
      this.dataPara.Combination = Combination;

      console.log('data upload:' + JSON.stringify(this.dataUpload)); */

      /* for (let i = 0; i < allTextLines.length; i++) {
        // split content based on comma
        let data = allTextLines[i].split(",");
        if (data.length === headers.length) {
          let tarr = [];
          for (let j = 0; j < headers.length; j++) {
            tarr.push(data[j]);
          }

          // log each row to see output
          console.log(tarr);
          lines.push(tarr);
        }
      } */
      // all rows in the csv file
      /* console.log(">>>>>>>>>>>>>>>>>", lines); */
    };
  }
}
