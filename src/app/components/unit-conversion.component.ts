import { UnitConversionService } from "./../service/unit-conversion.service";
import { Component, ViewChild, OnInit } from "@angular/core";
import { DataTableDirective } from "angular-datatables";
import { Subject } from "rxjs/Rx";

@Component({
  templateUrl: "unit-conversion.component.html"
})
export class UnitConversionComponent {
  allUnitConversionData = [];
  submitted = false;
  Convert_value: number;

  dataUnitConv = {
    from_unit: "",
    to_unit: "",
    factor: null,
    convert_value: 0
  };
  @ViewChild(DataTableDirective) datatableElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<String> = new Subject();

  constructor(private unitConvService: UnitConversionService) {
    this.dtOptions = {
      pagingType: "full_numbers"
    };
  }

  private ngOnInit() {
    this.unitConvService.getAllUnitConversion().subscribe(data => {
      this.allUnitConversionData = data;
      console.log("all unit conversion data");
      console.log(this.allUnitConversionData);
           this.dtTrigger.next();
    });
  }
  anAvail = null;
  addNew() {
    this.dataUnitConv.convert_value = +this.Convert_value;
    this.unitConvService.getAllUnitConversion().subscribe(data => {
      let count = 0;
      console.log('data threshold master:' + JSON.stringify(data));
      data.forEach(element => {
        if (
          this.dataUnitConv.from_unit === element.from_unit &&
          this.dataUnitConv.to_unit === element.to_unit &&
          this.dataUnitConv.convert_value === element.convert_value
        ) {
          count++;
        }
      });

      if (count > 0) {
        this.anAvail = true;
        setTimeout(() => {
          this.anAvail = false;
        }, 5000);
        this.submitted = false;
      } else {
    this.unitConvService
      .postUnitConversion(this.dataUnitConv)
      .subscribe(data => {
        console.log(data);
        window.location.reload();
      });
  }
  });
  }
}
