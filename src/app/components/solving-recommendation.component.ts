import { Component,ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs/Rx';
import { DataTableDirective } from 'angular-datatables';
import {SolvingRecomServ} from '../service/solving-recom.service'; 

@Component({
  selector: "app-post",
  templateUrl: 'solving-recommendation.component.html',
  providers: [SolvingRecomServ]
})
export class SolvingRecommendationComponent {
  @ViewChild(DataTableDirective) datatableElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<String> = new Subject();

  constructor(private sorem : SolvingRecomServ, public route : Router) { }

  contents:any[];
  idSolv= "";
  thresData: any[];
  recomData :any[];

  private ngOnInit(){
    this.sorem.getPara().subscribe(data => {
      console.log(data);
      this.contents = data;
      this.dtTrigger.next();
    });

    this.sorem.getMaxManyId().subscribe(data => {
      data.forEach(element => {
        console.log("data max id:" + JSON.stringify(element.max_solv_recom));
        let stringAboutChange = element.max_solv_recom;
        stringAboutChange = +stringAboutChange + 1;

        this.idSolv = stringAboutChange;
        console.log("new data" + JSON.stringify(this.idSolv));
      });
    });

    this.sorem.getThres().subscribe(data => {
      console.log(data);
      this.thresData = data;
    });

    this.sorem.getRecom().subscribe(data => {
      console.log(data);
      this.recomData = data;
    });
  }

  submitted = false;
  anAvail = null;
  dataSolv = {
    Solving_id: "",
    Threshold_id: "",
    Recommendation_id: ""
  };
  an: any;
  onChangeThres(index, item) {
    this.an = item;

    let Threshold_id = item.Threshold_id;

    this.an.Threshold_id = item.Threshold_id;
    console.log(this.an);

  }
  an2: any;
  onChangeRecom(index, item) {
    this.an2 = item;

    let Recommendation_id = item.Recommendation_id;
    let Recommendation = item.Recommendation;

    this.an2.Recommendation_id = item.Recommendation_id;
    this.an2.Recommendation = item.Recommendation;
    console.log(this.an2);

  }
  an3: any;
  onChangeThresEdit(index, item) {
    this.an3 = item;

    let Threshold_id = item.Threshold_id;

    this.an3.Threshold_id = item.Threshold_id;
    console.log(this.an3);

  }
  an4: any;
  onChangeRecomEdit(index, item) {
    this.an4 = item;

    let Recommendation_id = item.Recommendation_id;
    let Recommendation = item.Recommendation;

    this.an4.Recommendation_id = item.Recommendation_id;
    this.an4.Recommendation = item.Recommendation;
    console.log(this.an4);

  }
  addNewSolv() {
    this.submitted = true;

    let Solving_id = this.idSolv;
    /* Parameter_id = Parameter_id.charAt(0).toUpperCase() + Parameter_id.slice(1); */
    this.dataSolv.Solving_id = Solving_id;

    let Threshold_id = this.dataSolv.Threshold_id;
    this.dataSolv.Threshold_id = this.an.Threshold_id;

    let Recommendation_id = this.dataSolv.Recommendation_id;
    this.dataSolv.Recommendation_id = this.an2.Recommendation_id;


    console.log("data para:" + JSON.stringify(this.dataSolv));

    this.sorem.getPara().subscribe(data => {
      let count = 0;
      console.log('data solving recom:' + JSON.stringify(data));


      if (count > 0) {
        this.anAvail = true;
        setTimeout(() => {
          this.anAvail = false;
        }, 5000);
        this.submitted = false;
      } else {
        console.log("data para:" + JSON.stringify(this.dataSolv));
        this.sorem.postPara(this.dataSolv).subscribe(data => {
          alert("Your data has been added in database");
        }, window.location.reload());
      }
    });
  }

  // function delete
  contentTitleWillBeDeleted = "";
  dataContentToBeDeleted = { content: "", item: "" };
  willDeleteContent(content, item) {
    this.contentTitleWillBeDeleted = item.Solving_id;
    this.dataContentToBeDeleted.content = content;
    this.dataContentToBeDeleted.item = item;
  }
  deleteContent() {
    this.sorem
      .deletePara(this.dataContentToBeDeleted.item["Solving_id"])
      .subscribe(data => {
        console.log("Data yang akan dihapus" + JSON.stringify(this.dataContentToBeDeleted.item["Solving_id"]))
        alert("Your data has been deleted from database");
      },window.location.reload());
  }

  // function edit
  showEditContent = false;
  para: any;
  cancelEdit() {
    this.showEditContent = false;
  }
  willEditContent(content, item) {
    console.log(content, item);
    this.showEditContent = true;
    this.para = item;
  }
  editContent() {
    this.submitted = true;

    let Threshold_id = this.para.Threshold_id;
    this.para.Threshold_id = this.an3.Threshold_id;

    let Recommendation_id = this.para.Recommendation_id;
    this.para.Recommendation_id = this.an4.Recommendation_id;

    console.log("data para:" + JSON.stringify(this.para));

    this.sorem.getPara().subscribe(data => {
      let count = 0;


      if (count > 0) {
        this.anAvail = true;
        setTimeout(() => {
          this.anAvail = false;
        }, 5000);

        this.submitted = false;
      } else {
        this.sorem.putPara(this.para).subscribe(data => {
          alert("Your data has been updated in database");
        }, window.location.reload());
      }
    });
  }
}
