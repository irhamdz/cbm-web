import { Component,ViewChild } from '@angular/core';
import { PcarListService } from '../service/pcar-list.service';
import { Observable, Subject } from 'rxjs/Rx';
import { DataTableDirective } from 'angular-datatables'; 
import { Router } from '@angular/router';
import { ParameterService } from './parameter-master.service';

@Component({
  templateUrl: "pcarlist.component.html"
})



export class pcarlistComponent {
  @ViewChild(DataTableDirective)
    datatableElement: DataTableDirective;

siteId = "JBY";
    pcarListBySite = [];
  dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<String> = new Subject();

     constructor(public router: Router, private pcarlist: PcarListService) {
      this.dtOptions= {
      pagingType: 'full_numbers',
      autoWidth: false,
    };
      

    this.pcarlist.getPcarListBySite(this.siteId)
    .subscribe(data =>{
      this.pcarListBySite = data;
      this.dtTrigger.next();
    });
  }
}
