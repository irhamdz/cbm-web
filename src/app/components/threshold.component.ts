import { Component, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContentService } from '../service/content.service';
import { Observable, Subject } from 'rxjs/Rx';
import { DataTableDirective } from 'angular-datatables';
import { PcarService } from '../service/pcar.service';

@Component({
  templateUrl: 'threshold.component.html'
})
export class ThresholdComponent {
	@ViewChild(DataTableDirective)
  	datatableElement: DataTableDirective;


	dtOptions: DataTables.Settings = {};
  	dtTrigger: Subject<String> = new Subject();

	contents: any[];
	idThres = "";

	ThresWithPara: any[];

	critiData: any[];
		constructor(private pcarService: PcarService) {
  		 	this.dtOptions = {
			pagingType: 'full_numbers'
	};
				  /* this.pcarService.getAllThreshold()
					  .subscribe(data => {
						  console.log(data);
						  this.contents = data;
					  }); */

				  this.pcarService.getAllThresPara()
					  .subscribe(data => {
						  console.log(data);
						  this.ThresWithPara = data;
						  this.dtTrigger.next();
					  });


}
	private ngOnInit(){
	this.pcarService.getMaxThres().subscribe(data => {
		data.forEach(element => {
			console.log("data max id:" + JSON.stringify(element.max_threshold));
			let stringAboutChange = element.max_threshold.slice(3);
			stringAboutChange = +stringAboutChange + 1;

			let lengthStr = element.max_threshold.length; //length of str from max_param
			/* let lenghtNum = stringAboutChange.toString().length */
			let maxIdThres = element.max_threshold.slice(0, 3);

			let lengthOfZero = lengthStr - maxIdThres.length;
			lengthOfZero = stringAboutChange.toString().padStart(lengthOfZero, "0"); //add '0' basicly to length of str - maxIdParam.length
			this.idThres = maxIdThres + lengthOfZero;
			console.log("new data" + JSON.stringify(this.idThres));
		});
	});
	this.pcarService.getAllParameter().subscribe(data => {
		console.log(data);
		this.thresData = data;
	});
	this.pcarService.getAllCriticality().subscribe(data => {
		console.log(data);
		this.critiData = data;
	});
}

	an: any;
	onChangeThres(index, item) {
			this.an = item
			let Parameter_id = item.Parameter_id;
			let Parameter = item.Parameter;

			this.an.Parameter_id = item.Parameter_id;
			this.an.Parameter = item.Parameter;
			console.log(JSON.stringify(this.an));

	}

	an2: any;
	onChangeThresEdit(index, item) {
		this.an2 = item
		let Parameter_id = item.Parameter_id;
		let Parameter = item.Parameter;

		this.an2.Parameter_id = item.Parameter_id;
		this.an2.Parameter = item.Parameter;
		console.log(JSON.stringify(this.an2));

	}

	criticalData: any[];
	onChangeCri(index, item) {
		/* let source = item.trend_source;
		this.an.id = item.id; */
		console.log(item.Criticality);
		this.pcarService.getThresByCriticality(item.Criticality).subscribe(datas => {
			console.log("data Critical: " + JSON.stringify(item.Criticality));
			/* this.parameterTrend = this.parameterTrend; */
			this.criticalData = datas;
			this.dataThres.Criticality = item.Criticality;

		});
	}

	criticalDataEdit: any[];
	onChangeCriEdit(index, item) {
		/* let source = item.trend_source;
		this.an.id = item.id; */
		console.log(item.Criticality);
		this.pcarService.getThresByCriticality(item.Criticality).subscribe(datas => {
			console.log("data Critical: " + JSON.stringify(item.Criticality));
			/* this.parameterTrend = this.parameterTrend; */
			this.criticalDataEdit = datas;
			this.dataThres.Criticality = item.Criticality;

		});
	}

	thresData: any[];
	dataThres = {
		Threshold_id: "",
		Parameter_id: "",
		Criticality: "",
		Limit_Value: ""
	};
	anAvail = null;
	submitted = false;
	addNewThres() {
		this.submitted = true;

		let Threshold_id = this.idThres;
		/* Parameter_id = Parameter_id.charAt(0).toUpperCase() + Parameter_id.slice(1); */
		this.dataThres.Threshold_id = Threshold_id;

		let Parameter_id = this.dataThres.Parameter_id;
		this.dataThres.Parameter_id = this.an.Parameter_id;

		let Limit_Value = this.dataThres.Limit_Value;
		this.dataThres.Limit_Value = Limit_Value;

		console.log("data para:" + JSON.stringify(this.dataThres));

		this.pcarService.getThres().subscribe(data => {
			let count = 0;
			console.log('data threshold master:' + JSON.stringify(data));
			data.forEach(element => {
				if (
					this.dataThres.Criticality === element.Criticality &&
					this.dataThres.Parameter_id === element.Parameter_id	
				) {
					count++;
				}
			});

			if (count > 0) {
				this.anAvail = true;
				setTimeout(() => {
					this.anAvail = false;
				}, 5000);
				this.submitted = false;
			} else {
				console.log("data para:" + JSON.stringify(this.dataThres));
				this.pcarService.postThres(this.dataThres).subscribe(data => {
					alert("Your data has been added in database");
				}, window.location.reload());
			}
		});
	}

	// function delete
	contentTitleWillBeDeleted = "";
	dataContentToBeDeleted = { content: '', item: '' }
	willDeleteContent(content, item) {
		this.contentTitleWillBeDeleted = item.Parameter;
		this.dataContentToBeDeleted.content = content;
		this.dataContentToBeDeleted.item = item;
	}
	deleteContent() {
		this.pcarService.deleteThres(this.dataContentToBeDeleted.item["Threshold_id"])
			.subscribe(data => {
				alert("Your data has been deleted from database")
			},
			window.location.reload()
			)
	}
	//edit
	showEditContent = false;
	para: any;
	cancelEdit() {
		this.showEditContent = false;
	}
	willEditContent(content, item) {
		console.log(content, item);
		this.showEditContent = true;
		this.para = item;
	}
	editContent() {
		this.submitted = true;

		let Parameter_id = this.para.Parameter_id;
		this.para.Parameter_id = this.an2.Parameter_id;

		let Criticality = this.dataThres.Criticality;
		this.para.Criticality = Criticality;

		let Limit_Value = this.para.Limit_Value;
		this.para.Limit_Value = Limit_Value;

		
		this.pcarService.getThres()
			.subscribe(data => {
				let count = 0

				if (count > 0) {
					this.anAvail = true;
					setTimeout(() => {
						this.anAvail = false;
					}, 5000)

					this.submitted = false
				}else{
	
				this.pcarService.putThres(this.para).subscribe(data => {
					console.log("data para:" + JSON.stringify(this.para));
					alert("Your data has been updated in database");
				}, window.location.reload());
			}
	});
}


}
