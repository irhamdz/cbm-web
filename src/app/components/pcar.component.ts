import { URLSearchParams, Http } from "@angular/http";
import { Component, ViewChild } from "@angular/core";
import { DataMasterAdditional } from "./data-master-additional";
import { PcarService } from "../service/pcar.service";
import { Observable } from "rxjs/Observable";
import { DataTableDirective } from "angular-datatables";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { Subject } from "rxjs/Rx";
import { DomSanitizer } from "@angular/platform-browser";
@Component({
  templateUrl: "pcar.component.html"
})
export class PCARComponent {
  /* alertId = "ALERT0000001"; //todo remove
  alertUnitById = {}; //todo remove */
  alertUnit = [];
  alertParameter = [];
  alertUnitById = [];
  alertParameterById = [];
  alertAnalysisById = [];
  alertAnalysis: any;
  analysis = [];
  analysisAdd = [];
  recommendation = [];
  contentAnalysis = [];
  contentRecom = [];

  contentAnalysisAdd = [];
  contentRecomAdd = [];

  selectedSources = [];
  selectedSources2 = [];
  selectedSources3 = [];

  sourceSelected2 = false;
  sourceSelected3 = false;
  selectedParameter = [];

  selectedValue = null;
  selectedValue2 = null;
  selectedVal: any;
  submitted = false;
  submittedAR = false;
  an: any;
  anAdd: any;
  reAdd: any;
  an2: any;
  sub: any;
  isvalidated: any;
  alert_id = "";
  pcar_id = "";
  check: any = [];
  checkRec: any = [];
  checkAn: any = [];
  checkRe: any = [];
  formvalue: any;
  sourceSelected = false;
  parameterSelected = false;
  sourceTrend = [];
  parameterTrend = [];
  analysisDataMix = [];
  recomDataMix = [];
  links = [];
  linkNew = [];
  linkWithTicket = [];
  id = "";
  id2 = "";
  idRecomAdditional = "";
  idAnnAdditional = "";
  nrpkar = "90117173";
  analysisaddi = [];
  recommendationaddi = [];
  ticket = {
    username: "admindad"
  };
  tableauTicket = "";
  cobaUsername = "admindad";

  //add new analysis select and manual
  analysisSelect = false;
  analysisManual = false;
  recomSelect = false;
  recomManual = false;

  dataPCAR_Analysis = {
    pcar_id: "",
    analysis_id: "",
    marked: 0
  };

  dataPCAR_Recom = {
    pcar_id: "",
    recommendation_id: "",
    marked: 0
  };

  //pcar_analysis & pcar_recommendation by PCAR_id
  pcarAnalysisById = [];
  pcarRecommendationById = [];

  //proceed button
  proceed = false;
  proceedRec = false;

  @ViewChild(DataTableDirective) datatableElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};

  dataAnalysisAdditional = {
    id: "ANL_90117173_00001",
    description: "tes",
    association: "tes",
    model: "tes",
    status: "additional"
  };
  dataRecomAdditional = {
    id: "REC_90117173_00001",
    description: "tes",
    association: "tes",
    model: "tes",
    status: "additional"
  };
  dtTrigger: Subject<String> = new Subject();
  max;

  dataMA = [
    new DataMasterAdditional("Master"),
    new DataMasterAdditional("Additional")
  ];
  constructor(
    private http: Http,
    private pcarService: PcarService,
    private route: ActivatedRoute,
    private router: Router,
    private sanitizer: DomSanitizer
  ) {
    console.log("Pilih" + JSON.stringify(this.dataMA));

    this.pcarService.getMaxManyId().subscribe(data => {
      data.forEach(element => {
        console.log(
          "data max id Recom Add:" + JSON.stringify(element.max_reAdd)
        );
        console.log(
          "data max id Analysis Add:" + JSON.stringify(element.max_anAdd)
        );
        let maxIdreAdd = element.max_reAdd;
        let maxIdanAdd = element.max_anAdd;
        this.id = maxIdreAdd;
        this.id2 = maxIdanAdd;
        console.log("new data" + JSON.stringify(this.id));
        console.log("new data" + JSON.stringify(this.id2));
      });
    });

    this.pcarService.getAllAnalysis().subscribe(data => {
      console.log(data);
      this.analysis = data;
    });

    this.pcarService.getAllRecommendation().subscribe(data => {
      this.recommendation = data;
    });

    this.pcarService.getAnalysisAdd().subscribe(data => {
      if (data != "") {
        data.forEach(element => {
          console.log("data ambil analysis add :" + JSON.stringify(element.id)); //ANL_90117173_00001
          let idAnAdd = element.id.slice(13); //ANL00000001 => 0001

          idAnAdd = +idAnAdd + 1;

          let NRP = this.nrpkar + "_";

          let lengthStr = element.id.length;
          let maxIdAnnAdd = element.id.slice(0, 3) + "_" + NRP;

          let lengthofZero = lengthStr - maxIdAnnAdd.length;

          lengthofZero = idAnAdd.toString().padStart(lengthofZero, "0"); //add '0' basicly to length of str - maxIdParam.length
          this.idAnnAdditional = maxIdAnnAdd + lengthofZero;
          console.log(
            "For add analysis :" + JSON.stringify(this.idAnnAdditional) //max analysis additional + 1
          );
        });
      } else {
        this.pcarService
          .postAnalysisAdd(this.dataAnalysisAdditional)
          .subscribe(datas => {
            console.log(
              "data analysis additional cek : " + JSON.stringify(datas)
            );
          });
      }
      this.analysisaddi = data;
    });

    this.pcarService.getRecomAdd().subscribe(data => {
      if (data != "") {
        data.forEach(element => {
          console.log("data ambil recom id :" + JSON.stringify(element.id));

          let idRecomAdd = element.id.slice(13);
          idRecomAdd = +idRecomAdd + 1;

          let NRP = this.nrpkar + "_";

          let lengthStr = element.id.length;
          let maxIdRecomAdd = element.id.slice(0, 3) + "_" + NRP;

          let lengthofZero = lengthStr - maxIdRecomAdd.length;
          lengthofZero = idRecomAdd.toString().padStart(lengthofZero, "0"); //add '0' basicly to length of str - maxIdParam.length
          this.idRecomAdditional = maxIdRecomAdd + lengthofZero;
          console.log(
            "For add recom :" + JSON.stringify(this.idRecomAdditional)
          );
        });
      } else {
        this.pcarService
          .postRecomAdd(this.dataRecomAdditional)
          .subscribe(datas => {
            console.log(
              "data analysis additional cek : " + JSON.stringify(datas)
            );
          });
      }

      this.recommendationaddi = data;
    });

    this.pcarService.getAnalysisAdd().subscribe(data => {
      console.log(data);
      this.analysisAdd = data;
      this.dtTrigger.next();
    });
  }

  private ngOnInit() {
    let countAnalysis = 0;
    let countRecommend = 0;
    //get alert_id & pcar_id
    this.sub = this.route.params.subscribe(params => {
      this.pcar_id = params["PCAR_id"];
      this.alert_id = params["alert_id"]; // (+) converts string 'id' to a number
      console.log("data param:" + JSON.stringify(params));
    });

    this.pcarService.getMaxManyId().subscribe(data => {
      data.forEach(element => {
        console.log(
          "data max id Recom Add:" + JSON.stringify(element.max_reAdd)
        );
        console.log(
          "data max id Analysis Add:" + JSON.stringify(element.max_anAdd)
        );

        let maxIdreAdd = element.max_reAdd;
        let maxIdanAdd = element.max_anAdd;
        this.id = maxIdreAdd;
        this.id2 = maxIdanAdd;
        console.log("new data" + JSON.stringify(this.id));
        console.log("new data" + JSON.stringify(this.id2));
      });
    });

    this.pcarService.getAllSourceTrend().subscribe(data => {
      console.log(data);
      this.sourceTrend = data;
    });

    this.pcarService.getAllTrendData().subscribe(data => {
      this.parameterTrend = data;
    });

    this.pcarService.getAlertUnitById(this.alert_id).subscribe(data => {
      console.log(
        "data view alert + unit population by Id:" + JSON.stringify(data)
      );
      this.alertUnitById = data;
    });

    this.pcarService.getAlertParameterById(this.alert_id).subscribe(data => {
      console.log("data view alert + Parameter by Id:" + JSON.stringify(data));
      this.alertParameterById = data;
    });

    /* this.pcarService.getAlertAnalysisById(this.alert_id).subscribe(data => {
      console.log("data view alert analysis by Id:" + JSON.stringify(data));
      this.alertAnalysisById = data;
    }); */
    //get pcar by alert id
    this.pcarService.getPcarByAlertId(this.alert_id).subscribe(data => {
    console.log("data:" + JSON.stringify(data));
    this.alertAnalysisById = data;
    });

    //get pcar analysis & pcar recommendation by PCAR_id
    this.pcarService.getPcarAnalysisById(this.pcar_id).subscribe(data => {
      this.pcarAnalysisById = data;
      data.forEach(element => {
        if (element.marked === true) {
          countAnalysis++;
        }
        if (countAnalysis > 0) {
          this.proceed = true;
        } else {
          this.proceed = false;
        }
      });
    });
    this.pcarService.getPcarRecomById(this.pcar_id).subscribe(data => {
      this.pcarRecommendationById = data;
      data.forEach(element => {
        if (element.marked === true) {
          countRecommend++;
        }
         if (countRecommend > 0) {
           this.proceedRec = true;
         } else {
           this.proceedRec = false;
         }
      });
    });
  }

  onChangeAnalysis(index, item) {
    this.an = item;
    let id = item.id;
    this.an.id = item.id;
    console.log(this.an);
  }

  onChangeRecommendation(index, item) {
    this.an2 = item;
    let id = item.id;
    this.an2.id = item.id;
    console.log(this.an2);
  }
  selectCheckbox3(an: any) {
    /* let name = first + " " + last; */
    const len = this.check.length;
    let add = false;

    for (let i = 0; i < len; i++) {
      if (an.id === this.check[i].id) {
        add = true;

        if (i > -1) {
          this.check.splice(i, 1);
        }
      }
    }

    if (add === false) {
      this.check.push(an);
      this.pcarService.getRecomAdd().subscribe(data => {
        console.log(data);
        this.contentRecomAdd = data.sort((a, b) => {
          return a.description > b.description
            ? 1
            : b.description > a.description ? -1 : 0;
        });
      });
    }
  }

  selectCheckbox4(an: any) {
    /* let name = first + " " + last; */
    const len = this.check.length;
    let add = false;

    for (let i = 0; i < len; i++) {
      if (an.id === this.check[i].id) {
        add = true;

        if (i > -1) {
          this.check.splice(i, 1);
        }
      }
    }

    if (add === false) {
      this.check.push(an);
      this.pcarService.getAllRecommendation().subscribe(data => {
        console.log(data);
        this.contentRecom = data.sort((a, b) => {
          return a.recommendation > b.recommendation
            ? 1
            : b.recommendation > a.recommendation ? -1 : 0;
        });
      });
    }
  }

  selectCheckbox2(an: any) {
    /* let name = first + " " + last; */
    const len = this.checkAn.length;
    let add = false;

    for (let i = 0; i < len; i++) {
      if (an.id === this.checkAn[i].id) {
        add = true;

        if (i > -1) {
          this.checkAn.splice(i, 1);
        }
      }
    }

    if (add === false) {
      this.checkAn.push(an);
      this.pcarService.getAnalysisAdd().subscribe(data => {
        console.log(data);
        this.contentAnalysisAdd = data.sort((a, b) => {
          return a.description > b.description
            ? 1
            : b.description > a.description ? -1 : 0;
        });
      });
    }
  }

  selectCheckbox1(an: any) {
    /* let name = first + " " + last; */
    const len = this.checkAn.length;
    let add = false;

    for (let i = 0; i < len; i++) {
      if (an.id === this.checkAn[i].id) {
        add = true;

        if (i > -1) {
          this.checkAn.splice(i, 1);
        }
      }
    }

    if (add === false) {
      this.checkAn.push(an);
      this.pcarService.getAllAnalysis().subscribe(data => {
        console.log(data);
        this.contentAnalysis = data.sort((a, b) => {
          return a.analysis > b.analysis ? 1 : b.analysis > a.analysis ? -1 : 0;
        });
      });
    }
  }

  cancel() {
    this.router.navigate(["/components/pcarlist"]);
    /* window.location.reload(); */
  }

  dataPCAR = {
    PCAR_id: "PCAR-JBY-0000002",
    alert_id: "",
    analysis_id: "",
    recommendation_id: "",
    marked: 0,
    created: 0,
    validated: 0,
    executed: 0,
    completed: 0,
    date_time_created: null,
    date_time_validated: null,
    date_time_executed: null,
    date_time_completed: null
  };

  showAnalysisSelect = false;
  cancelSelectAnalysis() {
    this.showAnalysisSelect = false;
  }
  willSelectAnalysis() {
    this.showAnalysisManual = false;
    this.analysisManual = false;
    this.analysisSelect = true;
    this.showAnalysisSelect = true;
  }

  showAnalysisManual = false;
  cancelManualAnalysis() {
    this.showAnalysisManual = false;
  }
  willManualAnalysis() {
    this.analysisSelect = false;
    this.analysisManual = true;
    this.showAnalysisSelect = false;
    this.showAnalysisManual = true;
  }

  /* BATASSSSSSSSSSS */

  showRecomSelect = false;
  cancelSelectRecom() {
    this.showRecomSelect = false;
  }
  willSelectRecom() {
    this.recomSelect = true;
    this.recomManual = false;
    this.showRecomManual = false;
    this.showRecomSelect = true;
  }

  showRecomManual = false;
  cancelManualRecom() {
    this.showRecomManual = false;
  }
  willManualRecom() {
    this.recomSelect = false;
    this.recomManual = true;
    this.showRecomSelect = false;
    this.showRecomManual = true;
  }

  //tambah data
  dataRecomAdd = {
    id: "",
    description: "",
    association: "test",
    model: "PC200",
    status: "additional"
  };
  dataAnalysisAdd = {
    id: "",
    description: "",
    association: "test",
    model: "PC200",
    status: "additional"
  };
  dataPCAR2 = {
    PCAR_id: "PCAR-JBY-0000002",
    alert_id: "",
    analysis_id: "",
    recommendation_id: "",
    ismarked: 0,
    isvalidated: 0
  };
  dataPCAR4 = {
    PCAR_id: "PCAR-JBY-0000002",
    alert_id: "",
    analysis_id: "",
    recommendation_id: "",
    ismarked: 0,
    isvalidated: 0
  };
  dataPCAR3 = {
    PCAR_id: "PCAR-JBY-0000002",
    alert_id: "",
    analysis_id: "",
    recommendation_id: "",
    ismarked: 0,
    isvalidated: 0
  };
  addData() {
    this.submittedAR = true;

    console.log(this.analysisSelect);
    console.log(this.analysisManual);

    if (this.analysisSelect === true) {
      this.dataPCAR_Analysis.pcar_id = this.pcar_id;
      this.dataPCAR_Analysis.analysis_id = this.an.id;
      this.pcarService
        .postPcarAnalysis(this.dataPCAR_Analysis)
        .subscribe(data => {
          console.log(data);
        });
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    }

    if (this.analysisManual === true) {
      this.submittedAn = true;

      //add to analysis_additional_master
      let idann = this.dataAnalysisAdd.id;
      this.dataAnalysisAdd.id = this.idAnnAdditional;

      let description = this.dataAnalysisAdd.description;
      this.dataAnalysisAdd.description = description;

      this.pcarService.postAnalysisAdd(this.dataAnalysisAdd).subscribe(data => {
        console.log("data: " + JSON.stringify(this.dataAnalysisAdd));
      });

      //insert into PCAR_Analysis
      this.dataPCAR_Analysis.pcar_id = this.pcar_id;
      this.dataPCAR_Analysis.analysis_id = this.idAnnAdditional;
      /* console.log(this.dataPCAR_Analysis); */
      this.pcarService
        .postPcarAnalysis(this.dataPCAR_Analysis)
        .subscribe(data => {
          console.log(data);
        });
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    }

    /* let description = this.dataRecomAdd.description;
    this.dataRecomAdd.description = description;

    let descriptionRecom = this.dataAnalysisAdd.description;
    this.dataAnalysisAdd.description = descriptionRecom;

    ////////////////////////proses

    if (this.showAnalysisSelect === true && this.showRecomSelect === true) {
      let alert_id = this.dataPCAR.alert_id;
      this.dataPCAR.alert_id = this.alert_id;

      let analysis_id = this.dataPCAR.analysis_id;
      this.dataPCAR.analysis_id = this.an.id;

      let recommendation_id = this.dataPCAR.recommendation_id;
      this.dataPCAR.recommendation_id = this.an2.id;

      this.pcarService.postPcar(this.dataPCAR).subscribe(data => {
        console.log("data: " + JSON.stringify(this.dataPCAR));
      });
      window.location.reload();
    }

    if (this.showAnalysisManual === true && this.showRecomManual === true) {
      let idManRecom = this.dataPCAR4.recommendation_id;
      this.dataPCAR4.recommendation_id = this.idRecomAdditional;

      let alert_idan = this.dataPCAR4.alert_id;
      this.dataPCAR4.alert_id = this.alert_id;

      let idManAn = this.dataPCAR4.analysis_id;
      this.dataPCAR4.analysis_id = this.idAnnAdditional;

      this.pcarService.postPcar(this.dataPCAR4).subscribe(data => {
        console.log("data: " + JSON.stringify(this.dataPCAR4));
      });
      window.location.reload();
    }

    if (this.showAnalysisSelect === true && this.showRecomManual === true) {
      let alert_idan = this.dataPCAR3.alert_id;
      this.dataPCAR3.alert_id = this.alert_id;

      let id3 = this.dataPCAR3.analysis_id;
      this.dataPCAR3.analysis_id = this.an.id;

      let id4 = this.dataPCAR3.recommendation_id;

      this.dataPCAR3.recommendation_id = this.idRecomAdditional;

      this.pcarService.postPcar(this.dataPCAR3).subscribe(data => {
        console.log("data: " + JSON.stringify(this.dataPCAR3));
      });
      window.location.reload();
    }

    if (this.showAnalysisManual === true && this.showRecomSelect === true) {
      let alert_idre = this.dataPCAR2.alert_id;
      this.dataPCAR2.alert_id = this.alert_id;

      let id5 = this.dataPCAR2.recommendation_id;
      this.dataPCAR2.recommendation_id = this.an2.id;

      let id6 = this.dataPCAR2.analysis_id;
      this.dataPCAR2.analysis_id = this.idAnnAdditional;

      this.pcarService.postPcar(this.dataPCAR2).subscribe(data => {
        console.log("data: " + JSON.stringify(this.dataPCAR2));
      });
      window.location.reload();
    } */
  }

  addDataRec() {
    this.submittedAR = true;

    console.log(this.recomSelect);
    console.log(this.recomManual);

    if (this.recomSelect === true) {
      this.dataPCAR_Recom.pcar_id = this.pcar_id;
      this.dataPCAR_Recom.recommendation_id = this.an2.id;
      this.pcarService
        .postPcarRecom(this.dataPCAR_Recom)
        .subscribe(data => {
          console.log(data);
        });
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    }

    if (this.recomManual === true) {
      this.submittedAn = true;

      //add to recommendation_additional_master
      let idrecom = this.dataRecomAdd.id;
      this.dataRecomAdd.id = this.idRecomAdditional;

      let description = this.dataRecomAdd.description;
      this.dataRecomAdd.description = description;

      this.pcarService.postRecomAdd(this.dataRecomAdd).subscribe(data => {
        console.log("data: " + JSON.stringify(this.dataRecomAdd));
      });

      //insert into PCAR_Recommendation
      this.dataPCAR_Recom.pcar_id = this.pcar_id;
      this.dataPCAR_Recom.recommendation_id = this.idRecomAdditional;
      /* console.log(this.dataPCAR_Analysis); */
      this.pcarService
        .postPcarRecom(this.dataPCAR_Recom)
        .subscribe(data => {
          console.log(data);
        });
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    }
  }
  submittedAn = false;
  addAnalysisMan() {
    this.submittedAn = true;

    let idann = this.dataAnalysisAdd.id;
    this.dataAnalysisAdd.id = this.idAnnAdditional;

    let description = this.dataAnalysisAdd.description;
    this.dataAnalysisAdd.description = description;

    this.pcarService.postAnalysisAdd(this.dataAnalysisAdd).subscribe(data => {
      console.log("data: " + JSON.stringify(this.dataAnalysisAdd));
    });
  }
  submittedRe = false;
  addRecomMan() {
    this.submittedRe = true;

    let idrecom = this.dataRecomAdd.id;
    this.dataRecomAdd.id = this.idRecomAdditional;

    let description = this.dataRecomAdd.description;
    this.dataRecomAdd.description = description;

    this.pcarService.postRecomAdd(this.dataRecomAdd).subscribe(data => {
      console.log("data: " + JSON.stringify(this.dataRecomAdd));
    });
  }
  selectSourceTrue2() {
    this.sourceSelected2 = true;
    console.log("data selected source:" + this.sourceSelected2);
    /*  let lengthSourceSelected = this.selectedSources.length;
    console.log(lengthSourceSelected); */
    if (this.selectedSources2.length <= 1) {
      console.log("length: 1");
      let Master = this.selectedSources2.find(x => x == "Master");
      let Additional = this.selectedSources2.find(x => x == "Additional");
      if (Master != undefined) {
        console.log(Master);
        this.pcarService.getAllAnalysisMA(Master).subscribe(datas => {
          console.log("data Master: " + JSON.stringify(datas));
          this.analysisDataMix = datas;
        });
      } else if (Additional != undefined) {
        console.log("Additional");
        this.pcarService.getAllAnalysisMA(Additional).subscribe(datax => {
          console.log("data Additional: " + JSON.stringify(datax));
          this.analysisDataMix = datax;
        });
      }
    }
    setTimeout(() => {
      console.log("data analysis: " + JSON.stringify(this.analysisDataMix));
    }, 1000);
  }

  selectSourceTrue3() {
    this.sourceSelected3 = true;
    console.log("data selected source:" + this.sourceSelected3);
    /*  let lengthSourceSelected = this.selectedSources.length;
    console.log(lengthSourceSelected); */
    if (this.selectedSources3.length <= 1) {
      console.log("length: 1");
      let Master = this.selectedSources3.find(x => x == "Master");
      let Additional = this.selectedSources3.find(x => x == "Additional");
      if (Master != undefined) {
        console.log(Master);
        this.pcarService.getAllRecomMA(Master).subscribe(datas => {
          console.log("data Master: " + JSON.stringify(datas));
          this.recomDataMix = datas;
        });
      } else if (Additional != undefined) {
        console.log("Additional");
        this.pcarService.getAllRecomMA(Additional).subscribe(datax => {
          console.log("data Additional: " + JSON.stringify(datax));
          this.recomDataMix = datax;
        });
      }
    } else {
      console.log("length : 2");
      this.pcarService.getAllRecommendation().subscribe(dataz => {
        this.recomDataMix = dataz;
      });
    }
    setTimeout(() => {
      console.log("data analysis: " + JSON.stringify(this.recomDataMix));
    }, 1000);
  }

  selectSourceTrue() {
    this.sourceSelected = true;
    console.log("data selected source:" + this.selectedSources);
    /*  let lengthSourceSelected = this.selectedSources.length;
    console.log(lengthSourceSelected); */
    if (this.selectedSources.length <= 1) {
      console.log("length: 1");
      let VHMS = this.selectedSources.find(x => x == "VHMS");
      let PC2000 = this.selectedSources.find(x => x == "PC2000");
      if (VHMS != undefined) {
        console.log(VHMS);
        this.pcarService.getParameterBySource(VHMS).subscribe(datas => {
          console.log("data parameter: " + JSON.stringify(datas));

          /* this.parameterTrend = this.parameterTrend; */
          this.parameterTrend = datas;
        });
      } else if (PC2000 != undefined) {
        console.log("PC2000");
        this.pcarService.getParameterBySource(PC2000).subscribe(datax => {
          console.log("data parameter: " + JSON.stringify(datax));
          this.parameterTrend = datax;
        });
        /*  this.parameterTrend = this.parameterTrend.filter(x => x.trend_source == "PC2000"); */
      }
    } else {
      console.log("length : 2");
      this.pcarService.getAllTrendData().subscribe(dataz => {
        this.parameterTrend = dataz;
      });
    }
    setTimeout(() => {
      console.log(
        "data parameter trend: " + JSON.stringify(this.parameterTrend)
      );
    }, 1000);
  }

  tampilTableau() {
    this.parameterSelected = true;
    let observables = new Array();
    let linkNew = new Array();
    for (let data of this.selectedParameter) {
      observables.push(this.pcarService.postCustomTrendData(data));
    }
    console.log("data observables:" + JSON.stringify(observables));
    Observable.forkJoin(observables).subscribe(
      data => {
        this.links = data;
      },
      err => console.error(err)
    );

    /* this.parameterSelected = true;
    let observables = new Array();
    let linkNew = new Array();
    for (let data of this.selectedParameter) {
      observables.push(this.pcarService.getTrendDataByTrendname(data));
    }
    console.log("data observables:" + JSON.stringify(observables));
    Observable.forkJoin(observables).subscribe(
      data => {
        this.links = data;
        console.log("data links:" + this.links);
        for (var index = 0; index < this.links.length; index++) {
          let element = this.links[index];
          console.log(element);
          this.linkNew.push(element.link);
          console.log(element.link);
        }
      },
      err => console.error(err)
    ); */
    /*  res => console.log(res),
      error => console.log("Error: ", error) */
    setTimeout(() => {
      this.linkWithTicket = this.links;
      /* console.log("data links latest: " + JSON.stringify(this.links)); */
      /*  console.log("data links latest: " + JSON.stringify(this.linkNew));
     this.linkNew.forEach(element => {
       let tabServer = element.slice(0,19)
       let otherElement = element.slice(18)
        console.log('linknew latest:' + JSON.stringify(otherElement));
        this.linkWithTicket.push(tabServer + 'trusted/' + this.tableauTicket + otherElement);
      });
      console.log('new link with ticket:' + JSON.stringify(this.linkWithTicket)); */
    }, 1500);
  }

  selectCheckbox(an: any) {
    /* let name = first + " " + last; */
    let count = 0;
    const len = this.check.length;
    let add = false;

    if (add === false) {
      this.check.push(an);
    }
    console.log("coba passing :" + JSON.stringify(this.check));
    let updateItem = this.pcarAnalysisById.find(
      this.findIndexToUpdate,
      this.check.pcar_id
    );
    let index = this.pcarAnalysisById.indexOf(updateItem);

    //console.log(JSON.stringify('data update:' + updateItem));
    //console.log(JSON.stringify('index:' + index));
    //console.log('alert analysis baru:' + JSON.stringify(this.alertAnalysisById));

    this.pcarAnalysisById[index] = this.check;
    console.log(
      "pcar analysis latest:" + JSON.stringify(this.pcarAnalysisById)
    );

    this.pcarAnalysisById.forEach(element => {
      if (element.marked === true) {
        count++;
      }
    });

    if (count > 0) {
      this.proceed = true;
    }else{
      this.proceed = false;
    }
  }
  findIndexToUpdate(an: any) {
    return an.pcar_id === this;
  }

  selectCheckboxRec(rec: any) {
    /* let name = first + " " + last; */
    let count = 0;
    const len = this.checkRec.length;
    let add = false;

    if (add === false) {
      this.checkRec.push(rec);
    }
    console.log("coba passing :" + JSON.stringify(this.checkRec));
    let updateItem = this.pcarRecommendationById.find(
      this.findIndexToUpdateRec,
      this.checkRec.pcar_id
    );
    let index = this.pcarRecommendationById.indexOf(updateItem);

    //console.log(JSON.stringify('data update:' + updateItem));
    //console.log(JSON.stringify('index:' + index));
    //console.log('alert analysis baru:' + JSON.stringify(this.alertAnalysisById));

    this.pcarRecommendationById[index] = this.checkRec;
    console.log(
      "pcar recommendation latest:" +
        JSON.stringify(this.pcarRecommendationById)
    );

    this.pcarRecommendationById.forEach(element => {
      if (element.marked === true) {
        count++;
      }
    });

    if (count > 0) {
      this.proceedRec = true;
    } else {
      this.proceedRec = false;
    }
  }
  findIndexToUpdateRec(rec: any) {
    return rec.pcar_id === this;
  }

  close() {
    console.log("data check close" + JSON.stringify(this.pcarAnalysisById));
    console.log(
      "data check close" + JSON.stringify(this.pcarRecommendationById)
    );
    this.submitted = true;
    let observables = new Array();

    //update PCAR_Analysis
    for (let dataPCAR of this.pcarAnalysisById) {
      observables.push(this.pcarService.putPcarAnalysis(dataPCAR));
    }
    console.log("data observables:" + JSON.stringify(observables));
    Observable.forkJoin(observables).subscribe(
      res => console.log(res),
      error => console.log("Error: ", error)
    );

    //update PCAR_Recommendation
    for (let dataPCAR of this.pcarRecommendationById) {
      observables.push(this.pcarService.putPcarRecom(dataPCAR));
    }
    console.log("data observables:" + JSON.stringify(observables));
    Observable.forkJoin(observables).subscribe(
      res => console.log(res),
      error => console.log("Error: ", error)
    );

    this.router.navigate(["/components/pcarlist"]);
  }

  save() {
    console.log("data check" + JSON.stringify(this.alertAnalysisById));
    this.submitted = true;
    let observables = new Array();
    let dataval = new Array();

    //update PCAR_Analysis
    for (let dataPCAR of this.pcarAnalysisById) {
      observables.push(this.pcarService.putPcarAnalysis(dataPCAR));
    }
    console.log("data observables:" + JSON.stringify(observables));
    Observable.forkJoin(observables).subscribe(
      res => console.log(res),
      error => console.log("Error: ", error)
    );

    //update PCAR_Recommendation
    for (let dataPCAR of this.pcarRecommendationById) {
      observables.push(this.pcarService.putPcarRecom(dataPCAR));
    }
    console.log("data observables:" + JSON.stringify(observables));
    Observable.forkJoin(observables).subscribe(
      res => console.log(res),
      error => console.log("Error: ", error)
    );

    //update PCAR_table
    for (var index = 0; index < this.alertAnalysisById.length; index++) {
      this.alertAnalysisById[index].created = null;
      this.alertAnalysisById[index].validated = true;
      this.alertAnalysisById[index].scheduled = null;
      this.alertAnalysisById[index].executed = null;
      this.alertAnalysisById[index].completed = null;
      this.alertAnalysisById[index].date_time_validated = Date.now();
      console.log(this.alertAnalysisById[index]);
      this.pcarService
        .putPcar(this.alertAnalysisById[index])
        .subscribe(data => {
          console.log(data);
        });
    }
    /* for (let dataPCAR of this.alertAnalysisById) {
      const validated = dataPCAR.validated;
      dataPCAR.created = null;
      dataPCAR.validated = true;
      dataPCAR.scheduled = null;
      dataPCAR.executed = null;
      dataPCAR.completed = null;
      dataPCAR.date_time_validated = Date.now();
      observables.push(this.pcarService.putPcar(dataPCAR));
    }
    console.log("data observables:" + JSON.stringify(observables));
    Observable.forkJoin(observables).subscribe(
      res => console.log(res),
      error => console.log("Error: ", error)
    ); */
    this.router.navigate(["/components/pcarlist"]);
    /* setTimeout(() => {
      window.location.reload();
    }, 2000); */
    /*  window.alert("Data berhasil di save !");
    window.location.reload(); */
  }
  // lineChart
  public lineChartData: Array<any> = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: "Series A" },
    { data: [28, 48, 40, 19, 86, 27, 90], label: "Series B" },
    { data: [18, 48, 77, 9, 100, 27, 40], label: "Series C" }
  ];
  public lineChartLabels: Array<any> = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July"
  ];
  public lineChartOptions: any = {
    animation: false,
    responsive: true
  };
  public lineChartColours: Array<any> = [
    {
      // grey
      backgroundColor: "rgba(148,159,177,0.2)",
      borderColor: "rgba(148,159,177,1)",
      pointBackgroundColor: "rgba(148,159,177,1)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(148,159,177,0.8)"
    },
    {
      // dark grey
      backgroundColor: "rgba(77,83,96,0.2)",
      borderColor: "rgba(77,83,96,1)",
      pointBackgroundColor: "rgba(77,83,96,1)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(77,83,96,1)"
    },
    {
      // grey
      backgroundColor: "rgba(148,159,177,0.2)",
      borderColor: "rgba(148,159,177,1)",
      pointBackgroundColor: "rgba(148,159,177,1)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(148,159,177,0.8)"
    }
  ];

  public lineChartLegend: boolean = true;
  public lineChartType: string = "line";

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
}
