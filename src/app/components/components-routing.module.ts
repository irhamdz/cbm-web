import { UnitConversionComponent } from './unit-conversion.component';
import { UploadComponent } from './upload.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ButtonsComponent } from './buttons.component';
import { CardsComponent } from './cards.component';
import { FormsComponent } from './forms.component';
import { ModalsComponent } from './modals.component';
import { SocialButtonsComponent } from './social-buttons.component';
import { SwitchesComponent } from './switches.component';
import { TablesComponent } from './tables.component';
import { TabsComponent } from './tabs.component';

import { HomeComponent } from './home.component';
import { GeneralComponent } from './general.component';
import { HealthScoreComponent } from './health-score.component';
import { KPIComponent } from './kpi.component';
import { AlertComponent } from './alert.component';
import { PCARComponent } from './pcar.component';
import { ThresholdComponent } from './threshold.component';
import { AnalysisComponent } from './analysis-master.component';
import { RecommendationComponent } from './recommendation.component';
import { DataRequirementComponent } from './data-requirement.component';
import { ParameterMasterComponent } from './parameter-master.component';
import { SolvingMasterComponent } from './solving-master.component';
import { UserManagementComponent } from './user-management.component';
import { pcarlistComponent } from './pcarlist.component';
import { SolvingAnalysisComponent } from 'app/components/solving-analysis.component';
import { SolvingRecommendationComponent } from 'app/components/solving-recommendation.component';
import { LoginComponent } from 'app/pages/login.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Page'
    },
    children: [
      {
        path: 'buttons',
        component: ButtonsComponent,
        data: {
          title: 'Buttons'
        }
      },
      {
        path: 'cards',
        component: CardsComponent,
        data: {
          title: 'Cards'
        }
      },
      {
        path: 'forms',
        component: FormsComponent,
        data: {
          title: 'Forms'
        }
      },
      {
        path: 'modals',
        component: ModalsComponent,
        data: {
          title: 'Modals'
        }
      },
      {
        path: 'social-buttons',
        component: SocialButtonsComponent,
        data: {
          title: 'Social buttons'
        }
      },
      {
        path: 'switches',
        component: SwitchesComponent,
        data: {
          title: 'Switches'
        }
      },
      {
        path: 'tables',
        component: TablesComponent,
        data: {
          title: 'Tables'
        }
      },
      {
        path: 'tabs',
        component: TabsComponent,
        data: {
          title: 'Tabs'
        }
      },


      {
        path: 'home',
        component: HomeComponent,
        data: {
          title: 'Home'
        }
      },
      {
        path: 'general',
        component: GeneralComponent,
        data: {
          title: 'General'
        }
      },
      {
        path: 'health-score',
        component: HealthScoreComponent,
        data: {
          title: 'Health Score'
        }
      },
      {
        path: 'kpi',
        component: KPIComponent,
        data: {
          title: 'Key Performance Indicators'
        }
      },
      {
        path: 'alert',
        component: AlertComponent,
        data: {
          title: 'Alert'
        }
      },
      {
        path: 'pcar/:PCAR_id/:alert_id',
        component: PCARComponent,
        data: {
          title: 'Preventive & Corrective Action Request'
        }
      },
       {
        path: 'threshold',
        component: ThresholdComponent,
        data: {
          title: 'Threshold'
        }
      },
       {
        path: 'recommendation',
        component: RecommendationComponent,
        data: {
          title: 'Recommendation'
        }
      },
      {
        path: 'analysis-master',
        component: AnalysisComponent,
        data: {
          title: 'Analysis Master'
        }
      },
      {
        path: 'data-requirement',
        component: DataRequirementComponent,
        data: {
          title: 'Data Requirement'
        }
      },
      {
        path: 'parameter-master',
        component: ParameterMasterComponent,
        data: {
          title: 'Parameter Master'
        }
      },
      {
        path: 'solving-master',
        component: SolvingMasterComponent,
        data: {
          title: 'Solving Master'
        }
      },
      {
        path: 'solving-analysis',
        component: SolvingAnalysisComponent,
        data: {
          title: 'Solving Analysis'
        }
      },
      {
        path: 'solving-recommendation',
        component: SolvingRecommendationComponent,
        data: {
          title: 'Solving Recommendation'
        }
      },
      {
        path: 'user-management',
        component: UserManagementComponent,
        data: {
          title: 'User Management'
        }
      },
      {
        path: 'pcarlist',
        component: pcarlistComponent,
        data: {
          title: 'PCAR List'
        }
      },
      {
        path: 'unit-conversion',
        component: UnitConversionComponent,
        data: {
          title: 'Unit Conversion'
        }
      },
      {
        path: 'upload',
        component: UploadComponent,
        data: {
          title: 'Demo Upload'
        }
      },
      {
        path: 'login',
        component: LoginComponent,
        data: {
          title: 'Log In'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule {}
