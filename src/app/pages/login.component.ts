import { Http } from "@angular/http";
import { Component } from "@angular/core";

@Component({
  templateUrl: "login.component.html"
})
export class LoginComponent {
  loading = false;
  dataLogin = {
    UserID: "",
    Password: "",
    ApplicationID: "CBM"
  };

  constructor(private http: Http) {}

  login() {
    console.log("dataLogin:" + JSON.stringify(this.dataLogin));

    this.loading = true;
    this.http
      .post("http://utmiddleware.unitedtractors.com/User/api/user/login/", this.dataLogin)
      .map(res => res.json())
      .subscribe(data => {
          console.log("coba login");
          console.log(data);
        }, err => {
          console.log(err);
        });
  }
}
