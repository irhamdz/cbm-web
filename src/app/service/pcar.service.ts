import { Injectable } from "@angular/core";
import { Headers, Http, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { URLSearchParams } from "@angular/http";
import "rxjs/Rx";
import "rxjs/add/operator/map";

@Injectable()
export class PcarService {
  /* _errorhandler: any; */
  alertId = "ALERT0000001";
  urlAlert_Unit = "http://cbm-api.unitedtractors.com/api/vw_alert_units/";
  urlAlert_Parameter = "http://cbm-api.unitedtractors.com/api/vw_alert_parameters/";
  urlMaxIdPcar = "http://cbm-api.unitedtractors.com/api/max_id_pcars";
  urlAlert_Analysis = "http://cbm-api.unitedtractors.com/api/alert_pcars";
  urlAnalysis = "http://cbm-api.unitedtractors.com/api/analysis_master_tables";
  urlAnalysisAdd = "http://cbm-api.unitedtractors.com/api/analysis_master_additionals";
  urlRecommendation = "http://cbm-api.unitedtractors.com/api/recommendation_master_tables";
  urlPcar = "http://cbm-api.unitedtractors.com/api/pcar_tables/";
  urlRecommendationAdd = "http://cbm-api.unitedtractors.com/api/recommendation_master_additionals";

  urlLastAdd = "http://cbm-api.unitedtractors.com/api/max_many_additionals";
  urlSourceTrend = "http://cbm-api.unitedtractors.com/api/Source_Trends/";
  urlTrendData = "http://cbm-api.unitedtractors.com/api/Trend_Data/";
  urlanalysisMA = "http://cbm-api.unitedtractors.com/api/vw_analysis_MAs";
  urlrecomMA = "http://cbm-api.unitedtractors.com/api/vw_recom_MAs";
  urlTableauTicket = "http://cbm-api.unitedtractors.com/api/services/tableauTicket/";
  urlCustomTrend = "http://cbm-api.unitedtractors.com/api/custom_trend_data/getLink";

  urlThreshold = "http://cbm-api.unitedtractors.com/api/threshold_master_tables/";
  urlMaxThres = "http://cbm-api.unitedtractors.com/api/max_thresholds/";
  urlParameter = "http://cbm-api.unitedtractors.com/api/parameter_master_tables/";
  urlDisThres = "http://cbm-api.unitedtractors.com/api/distinct_thresholds/";

  //PCAR ANALYSIS and PCAR RECOMMENDATION
  urlPcarAnalysis = "http://cbm-api.unitedtractors.com/api/PCAR_Analyses/";
  urlPcarRecommendation = "http://cbm-api.unitedtractors.com/api/PCAR_Recommendations/";
  urlVWPcarAnalysis = "http://cbm-api.unitedtractors.com/api/vw_PCAR_Analyses/";
  urlVWPcarRecommendation = "http://cbm-api.unitedtractors.com/api/vw_PCAR_Recommendations/";

  urlSolvingMax = "http://cbm-api.unitedtractors.com/api/max_solvings";
  urlVWParaThres = "http://cbm-api.unitedtractors.com/api/vw_parameter_thresholds/";

  //solving master
  urlSolvingMaster = "http://cbm-api.unitedtractors.com/api/vw_solving_masters/";
  urlSolvingMasterWithDesc = "http://localhost:3000/api/vw_solvingMaster_withDescs";

  constructor(public http: Http) {
    this.http = http;
    console.log("Hello PcarService Provider");
  }
  getMaxThres() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlMaxThres)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  getMaxSolv() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlSolvingMax)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  getMaxManyId() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlLastAdd)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }
  getAlertUnitById(alert_id) {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlAlert_Unit + alert_id)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  getAlertParameterById(alert_id) {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlAlert_Parameter + alert_id)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }
  getAlertAnalysisById(alert_id) {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlAlert_Analysis + "?filter[where][Alert_id]=" + alert_id)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }
  getAllThreshold() {
    return Observable.create(observer => {
      return this.http
        .get(this.urlThreshold)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }

  getAllThresPara() {
    return Observable.create(observer => {
      return this.http
        .get(this.urlVWParaThres)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }

  putThres(para) {
    return Observable.create(observer => {
      return this.http
        .patch(this.urlThreshold + "/" + para.Threshold_id, para)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }

  getAllAnalysis() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlAnalysis)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  postAnalysis(dataAn) {
    return Observable.create(observer => {
      return this.http
        .post(this.urlAnalysis, dataAn)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }

  getAnalysisAdd() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlAnalysisAdd)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  postAnalysisAdd(dataAll) {
    return Observable.create(observer => {
      return this.http
        .post(this.urlAnalysisAdd, dataAll)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }
  getRecomAdd() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlRecommendationAdd)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  postRecomAdd(dataAll) {
    return Observable.create(observer => {
      return this.http
        .post(this.urlRecommendationAdd, dataAll)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }
  getPcar() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlPcar)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  getThres() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlThreshold)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }
  getThresByCriticality(Criticality) {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlDisThres + "?filter[where][Criticality]=" + Criticality)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }
  postThres(dataThres) {
    return Observable.create(observer => {
      return this.http
        .post(this.urlThreshold, dataThres)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }
  postPcar(dataPCAR) {
    return Observable.create(observer => {
      return this.http
        .post(this.urlPcar, dataPCAR)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }

  getAllRecommendation() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlRecommendation)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  getMaxIdPcar() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlMaxIdPcar)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  putPcar(data) {
    return Observable.create(observer => {
      return this.http
        .patch(this.urlPcar + data.new_id, data)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }

  putPcarValidated(data) {
    return Observable.create(observer => {
      return this.http
        .patch(this.urlPcar + data.id, data)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }

  getAllSourceTrend() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlSourceTrend)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  getAllCriticality() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlDisThres)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }
  getAllTrendData() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlTrendData)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  getAllParameter() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlParameter)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  getParameterBySource(trend_source) {
    return Observable.create(observer => {
      return (
        this.http
          .get(
            this.urlTrendData + "?filter[where][trend_source]=" + trend_source
          )
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  getAllAnalysisMA(status) {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlanalysisMA + "?filter[where][status]=" + status)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  getAllRecomMA(status) {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlrecomMA + "?filter[where][status]=" + status)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  getTrendDataByTrendname(trend_name) {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlTrendData + trend_name)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  postTicket(username) {
    return Observable.create(observer => {
      return this.http
        .post(this.urlTableauTicket, username)
        .map(res => res.text());
      /* .subscribe(data => {
           observer.next(data);
           observer.complete();
         }); */
    });
  }

  postCustomTrendData(trend_name) {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlCustomTrend + "?trend_name=" + trend_name)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  /* (response: Response) => {
        let user = response.json();} */

  /* getTicketTableau(param) {
    return Observable.create(observer => {
      return this.http
        .post("http://10.2.51.103/trusted", param)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    }); 
  } */

  deleteThres(Threshold_id) {
    return Observable.create(observer => {
      return this.http
        .delete(this.urlThreshold + Threshold_id)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }

  //get PCAR analysis by pcar_id
  getPcarAnalysisById(param) {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlVWPcarAnalysis + "?filter[where][pcar_id]=" + param)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  //get PCAR recommendation by pcar_id
  getPcarRecomById(param) {
    return Observable.create(observer => {
      return (
        this.http
          .get(
            this.urlVWPcarRecommendation + "?filter[where][pcar_id]=" + param
          )
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  //update PCAR_Analysis by pcar_id
  putPcarAnalysis(data) {
    return Observable.create(observer => {
      return this.http
        .patch(this.urlPcarAnalysis + data.id, data)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }

  //update PCAR_Recommendation by pcar_id
  putPcarRecom(data) {
    return Observable.create(observer => {
      return this.http
        .patch(this.urlPcarRecommendation + data.id, data)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }

  //insert PCAR_Analysis
  postPcarAnalysis(data) {
    return Observable.create(observer => {
      return this.http
        .post(this.urlPcarAnalysis, data)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }

  //insert PCAR_Recommendation
  postPcarRecom(data) {
    return Observable.create(observer => {
      return this.http
        .post(this.urlPcarRecommendation, data)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }

  getPcarByAlertId(alert_id) {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlPcar + "?filter[where][alert_id]=" + alert_id)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  getAllAnalysisMasAdd() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlanalysisMA)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  getAllRecomMasAdd() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlrecomMA)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  getAllSolvingMaster() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlSolvingMaster)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  getAllSolvingMasterWithDesc() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlSolvingMasterWithDesc)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }
}
