import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class SolvingRecomServ {
  title = "app works!";
  urlSolRecom = "http://cbm-api.unitedtractors.com/api/solving_recommendations/";
  urlMax = "http://cbm-api.unitedtractors.com/api/max_solvingrecoms/";
  urlRecom = "http://cbm-api.unitedtractors.com/api/recommendation_master_tables/";
  urlThresh = "http://cbm-api.unitedtractors.com/api/threshold_master_tables/";

  constructor(public http: Http) {
    console.log("Hello fellow user");
  }
  getThres() {
      return Observable.create(observer => {
          return (
              this.http
                  .get(this.urlThresh)
                  .map(res => res.json())
                  // .catch(this.handleError)
                  .subscribe(data => {
                      // this.data = data;
                      observer.next(data);
                      observer.complete();
                  })
          );
      });
  }
  getRecom() {
      return Observable.create(observer => {
          return (
              this.http
                  .get(this.urlRecom)
                  .map(res => res.json())
                  // .catch(this.handleError)
                  .subscribe(data => {
                      // this.data = data;
                      observer.next(data);
                      observer.complete();
                  })
          );
      });
  }
  getMaxManyId() {
      return Observable.create(observer => {
          return (
              this.http
                  .get(this.urlMax)
                  .map(res => res.json())
                  // .catch(this.handleError)
                  .subscribe(data => {
                      // this.data = data;
                      observer.next(data);
                      observer.complete();
                  })
          );
      });
  }

  getPara() {
    return Observable.create(observer => {
      return (
        this.http
              .get(this.urlSolRecom)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  deletePara(Solving_id) {
    return Observable.create(observer => {
      return this.http
          .delete(this.urlSolRecom + Solving_id)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }
  putPara(para) {
    return Observable.create(observer => {
      return this.http
          .patch(this.urlSolRecom + para.Solving_id, para)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }
  postPara(dataPara) {
    return Observable.create(observer => {
      return this.http
          .post(this.urlSolRecom, dataPara)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }
 
}