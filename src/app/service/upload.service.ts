import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/Rx";
import "rxjs/add/operator/map";

@Injectable()
export class UploadService {
  dataTransactionUrl = "http://localhost:3000/api/data_transactions/";
  constructor(public http: Http) {}

  postUpload(data) {
    return Observable.create(observer => {
      return this.http
        .post(this.dataTransactionUrl, data)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }
}
