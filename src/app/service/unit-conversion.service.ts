import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/Rx";
import "rxjs/add/operator/map";

@Injectable()
export class UnitConversionService {
  unitConversionUrl = "http://cbm-api.unitedtractors.com/api/Unit_Conversions/";
  constructor(public http: Http) {}

  getAllUnitConversion() {
    return Observable.create(observer => {
      return this.http
        .get(this.unitConversionUrl)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }

  postUnitConversion(data) {
    return Observable.create(observer => {
      return this.http
        .post(this.unitConversionUrl, data)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }

  putUnitConversion(data) {
    return Observable.create(observer => {
      return this.http
        .patch(this.unitConversionUrl + data.id, data)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }

  deleteUnitConversion(id) {
    return Observable.create(observer => {
      return this.http
        .delete(this.unitConversionUrl + id)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }
}

