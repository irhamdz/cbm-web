import { TestBed, inject } from '@angular/core/testing';

import { PcarService } from './pcar.service';

describe('PcarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PcarService]
    });
  });

  it('should ...', inject([PcarService], (service: PcarService) => {
    expect(service).toBeTruthy();
  }));
});
