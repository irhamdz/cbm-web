import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class ContentService {
  // recommendationurl='http://nodejs.unitedtractors.com:3000/api/recommendation_master_tables';
  // analysisurl='http://nodejs.unitedtractors.com:3000/api/analysis_master_tables';


  recommendationurl='https://dca-loopback-api.us-east.mybluemix.net/api/pcar_recommendation_masters';
  analysisurl='https://dca-loopback-api.us-east.mybluemix.net/api/pcar_analysis_masters';
  constructor(public http: Http) {
    this.http = http;
    console.log('Hello ContentService Provider');
  }

getRecommendation(){
    return Observable.create(observer => {
      return this.http.get(this.recommendationurl)
      .map(res => res.json())
      // .catch(this.handleError)
      .subscribe(data => {
        // this.data = data;wd
        console.log(data);
        observer.next(data);
        observer.complete();
      })
    })
  }


  getAnalysis(){
    return Observable.create(observer => {
      return this.http.get(this.analysisurl)
      .map(res => res.json())
      // .catch(this.handleError)
      .subscribe(data => {
        // this.data = data;wd
        console.log(data);
        observer.next(data);
        observer.complete();
      })
    })
  }
  }  