import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class PcarListService {
  urlPCARlist = "http://cbm-api.unitedtractors.com/api/vw_distinct_pcar_lists/";
  /* urlPCARlist = "http://localhost:3000/api/vw_distinct_pcar_lists/"; */

  constructor(public http: Http) {
    this.http = http;
    console.log("Hello PcarService Provider");
  }

  getPcarList() {
    return Observable.create(observer => {
      return (
        this.http
          .get(this.urlPCARlist)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  getPcarListBySite(parameter) {
    return Observable.create(observer => {
      return (
        this.http
          .get(
            this.urlPCARlist +
              "?filter[where][PCAR_id][like]=%" +
              parameter +
              "%"
          )
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }
}
