import { TestBed, inject } from '@angular/core/testing';

import { PcarListService } from './pcar-list.service';

describe('PcarListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PcarListService]
    });
  });

  it('should ...', inject([PcarListService], (service: PcarListService) => {
    expect(service).toBeTruthy();
  }));
});
