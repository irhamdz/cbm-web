import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class SolvingAnalysisServ {
  title = "app works!";
  urlSolAn = "http://cbm-api.unitedtractors.com/api/solving_analyses/";
  urlMax = "http://cbm-api.unitedtractors.com/api/max_solvinganalyses/";
  urlAnalysis = "http://cbm-api.unitedtractors.com/api/analysis_master_tables/";
  urlThresh = "http://cbm-api.unitedtractors.com/api/threshold_master_tables/";

  constructor(public http: Http) {
    console.log("Hello fellow user");
  }
  getThres() {
      return Observable.create(observer => {
          return (
              this.http
                  .get(this.urlThresh)
                  .map(res => res.json())
                  // .catch(this.handleError)
                  .subscribe(data => {
                      // this.data = data;
                      observer.next(data);
                      observer.complete();
                  })
          );
      });
  }
  getAnalysis() {
      return Observable.create(observer => {
          return (
              this.http
                  .get(this.urlAnalysis)
                  .map(res => res.json())
                  // .catch(this.handleError)
                  .subscribe(data => {
                      // this.data = data;
                      observer.next(data);
                      observer.complete();
                  })
          );
      });
  }
  getMaxManyId() {
      return Observable.create(observer => {
          return (
              this.http
                  .get(this.urlMax)
                  .map(res => res.json())
                  // .catch(this.handleError)
                  .subscribe(data => {
                      // this.data = data;
                      observer.next(data);
                      observer.complete();
                  })
          );
      });
  }

  getPara() {
    return Observable.create(observer => {
      return (
        this.http
              .get(this.urlSolAn)
          .map(res => res.json())
          // .catch(this.handleError)
          .subscribe(data => {
            // this.data = data;
            observer.next(data);
            observer.complete();
          })
      );
    });
  }

  deletePara(Solving_id) {
    return Observable.create(observer => {
      return this.http
          .delete(this.urlSolAn + Solving_id)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }
  putPara(para) {
    return Observable.create(observer => {
      return this.http
          .patch(this.urlSolAn + para.Solving_id, para)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }
  postPara(dataPara) {
    return Observable.create(observer => {
      return this.http
          .post(this.urlSolAn, dataPara)
        .map(res => res.json())
        .subscribe(data => {
          observer.next(data);
          observer.complete();
        });
    });
  }
 
}